﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 18: Many-Worlds Interpretation")]
	public class Day18
	{
		[TestCase(TestInput1, ExpectedResult = 8)]
		[TestCase(TestInput2, ExpectedResult = 86)]
		[TestCase(TestInput3, ExpectedResult = 132)]
		[TestCase(TestInput4, ExpectedResult = 136)]
		[TestCase(TestInput5, ExpectedResult = 81)]
		[TestCase(TestInput6, ExpectedResult = 20)]
		[TestCase(RealInput1, ExpectedResult = 4228)]
		public int Part1(string input) => new Dungeon(input, 1).FindPathLength();

		[TestCase(TestInput7, ExpectedResult = 8)]
		[TestCase(TestInput8, ExpectedResult = 24)]
		[TestCase(TestInput9, ExpectedResult = 32)]
		[TestCase(TestInput10, ExpectedResult = 72)]
		[TestCase(RealInput2, ExpectedResult = 1858)]
		public int Part2(string input)
		{
			var dungeon = new Dungeon(input, 4);
			var (len, path) = FindOptimalPath(dungeon);
			Console.WriteLine($"{len}: {path}");
			/*
			Console.WriteLine(dungeon);
			foreach (var key in path)
			{
				dungeon = dungeon.MoveTo(key);
				Console.WriteLine(dungeon);
			}
			*/
			return len;
		}

		private static (int length, string path) FindOptimalPath(Dungeon dungeon)
		{
			var shortestPathLen = int.MaxValue;
			var shortestPath = "";
			foreach (var (len, path) in BuildPath(dungeon, new Dictionary<int, Dictionary<string, (int minLen, string keyOrder)>>()))
			{
				if (len < shortestPathLen)
				{
					shortestPathLen = len;
					shortestPath = path;
				}
			}
			return (shortestPathLen, shortestPath);
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		private static IEnumerable<(int length, string path)> BuildPath(Dungeon dungeon, Dictionary<int, Dictionary<string, (int minLen, string keyOrder)>> subPathLengths)
		{
			var reachable = dungeon.FindReachable();
			if (dungeon.PickedUpKeys.Length + 1 == dungeon.Keys.Count)
			{
				var (key, len) = reachable.First();
				if (!subPathLengths.TryGetValue(dungeon.GetCurPosHash(), out var subPathKeysCache))
					subPathLengths[dungeon.GetCurPosHash()] = subPathKeysCache = new Dictionary<string, (int, string)>(1);
				subPathKeysCache[dungeon.PickedUpKeysSorted] = (len, key.ToString());
				yield return (len, key.ToString());
			}
			else if (subPathLengths.TryGetValue(dungeon.GetCurPosHash(), out var cachedSubPath)
					 && cachedSubPath.TryGetValue(dungeon.PickedUpKeysSorted, out var subPathInfo))
			{
				yield return (subPathInfo.minLen, subPathInfo.keyOrder);
			}
			else
			{
				(int length, string path) shortestSubPath = (int.MaxValue, null);
				foreach (var (key, len) in reachable)
				foreach (var p in BuildPath(dungeon.MoveTo(key), subPathLengths))
				{
					var cLen = p.length + len;
					if (cLen < shortestSubPath.length)
						shortestSubPath = (len + p.length, key + p.path);
				}
				if (cachedSubPath == null)
					subPathLengths[dungeon.GetCurPosHash()] = cachedSubPath = new Dictionary<string, (int minLen, string keyOrder)>();
				cachedSubPath[dungeon.PickedUpKeysSorted] = (shortestSubPath.length, shortestSubPath.path);
				yield return shortestSubPath;
			}
		}

		private class Dungeon
		{
			public Dungeon(string input, int robotNumber)
			{
				var lines = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
				CurPos = new (int x, int y)[robotNumber];
				Dimensions = (lines[0].Length, lines.Length);
				Middle = (Dimensions.x / 2, Dimensions.y / 2);
				map = new char[Dimensions.y, Dimensions.x];
				var robot = 0;
				for (var y = 0; y < Dimensions.y; y++)
				for (var x = 0; x < Dimensions.x; x++)
				{
					var c = lines[y][x];
					map[y, x] = c;
					if (c != '#')
						FreeSpaces++;
					switch (c)
					{
							case '#':
								break;
							case '.':
							case ' ':
								map[y, x] = ' ';
								break;
							case '@':
								map[y, x] = ' ';
								CurPos[robot++] = (x, y);
								break;
							case char k when k >='a' && k <='z':
								Keys[k] = (x, y);
								break;
							case char d when d >= 'A' && d <= 'Z':
								Doors[d] = (x, y);
								break;
							default:
								throw new DataException("Unknown dungeon symbol " + c);
					}
				}
				for (var i = 0; i < CurPos.Length; i++)
				{
					foreach (var (k, _) in FindAllTheoreticallyReachable(CurPos[i]))
						KeyToQuadrantMap[k] = i;
				}
				front = new HashSet<(int x, int y)>(FreeSpaces);
				nextFront = new HashSet<(int x, int y)>(FreeSpaces);
				visited = new Dictionary<(int x, int y), int>(FreeSpaces);
			}

			private Dungeon(Dungeon dungeon, char pickupKey)
			{
				Dimensions = dungeon.Dimensions;
				Middle = dungeon.Middle;
				map = dungeon.map;
				CurPos = dungeon.CurPos.ToArray();
				CurPos[dungeon.GetQuadrant(pickupKey)] = dungeon.Keys[pickupKey];
				FreeSpaces = dungeon.FreeSpaces;
				Keys = dungeon.Keys;
				Doors = dungeon.Doors;
				KeyToQuadrantMap = dungeon.KeyToQuadrantMap;
				PickedUpKeys = dungeon.PickedUpKeys + pickupKey;
				PickedUpKeysSorted = new string(PickedUpKeys.OrderBy(c => c).ToArray());
				OpenedDoors = dungeon.OpenedDoors + (char)(pickupKey - 'a' + 'A');
				reachableCache = dungeon.reachableCache;
				front = dungeon.front;
				nextFront = dungeon.nextFront;
				visited = dungeon.visited;
			}

			private readonly int FreeSpaces;
			private readonly char[,] map;
			private readonly (int x, int y) Dimensions;
			private readonly (int x, int y) Middle;
			public readonly (int x, int y)[] CurPos;
			public readonly Dictionary<char, (int x, int y)> Keys = new Dictionary<char, (int x, int y)>(32);
			public readonly Dictionary<char, (int x, int y)> Doors = new Dictionary<char, (int x, int y)>(32);
			public readonly Dictionary<char, int> KeyToQuadrantMap = new Dictionary<char, int>(32);
			public ref char this[(int x, int y) c] => ref map[c.y, c.x];
			private readonly string OpenedDoors = "";
			public readonly string PickedUpKeys = "";
			public readonly string PickedUpKeysSorted = "";
			private readonly Dictionary<(int x, int y), Dictionary<string, List<(char key, int length)>>> reachableCache = new Dictionary<(int x, int y), Dictionary<string, List<(char key, int length)>>>(32);
			private HashSet<(int x, int y)> front;
			private HashSet<(int x, int y)> nextFront;
			private Dictionary<(int x, int y), int> visited;

			public int GetQuadrant(char key) => KeyToQuadrantMap[key];
			public int GetCurPosHash() => CurPos.Length == 1 ? CurPos[0].GetHashCode() : HashCode.Combine(CurPos[0], CurPos[1], CurPos[2], CurPos[3]);
			private bool IsDoor(char c) => Doors.ContainsKey(c);
			private bool IsKey(char c) => Keys.ContainsKey(c);
			private bool CanOpen(char c) => OpenedDoors.Contains(c);
			private bool UsedKey(char c) => PickedUpKeys.Contains(c);
			private bool CanTheoreticallyMove((int x, int y) c) => this[c] != '#';
			[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
			private bool CanMove((int x, int y) c)
			{
				ref var s = ref map[c.y, c.x];
				if (s == '#')
					return false;

				return s == ' ' || IsKey(s) || CanOpen(s);
			}
			private bool CanMove((int x, int y, string z) c)
			{
				ref var s = ref map[c.y, c.x];
				if (s == '#')
					return false;

				return s == ' ' || IsKey(s) || c.z.Contains((char)(s-'A'+'a'));
			}

			public IEnumerable<(char key, int length)> FindReachable() => CurPos.SelectMany(FindReachable);

			[MethodImpl(MethodImplOptions.AggressiveOptimization)]
			private List<(char key, int length)> FindReachable((int x, int y) o)
			{
				if (reachableCache.TryGetValue(o, out var cachedState)
					&& cachedState.TryGetValue(PickedUpKeysSorted, out var cachedPath))
					return cachedPath;

				front.Clear();
				front.Add(o);
				nextFront.Clear();
				visited.Clear();
				visited[o] = 0;
				int wave = 0;
				var result = new List<(char key, int length)>(Keys.Count);
				do
				{
					nextFront.Clear();
					wave++;
					foreach (var c in front)
					{
						var nc = (c.x - 1, c.y);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, wave));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x, c.y - 1);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, wave));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x + 1, c.y);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, wave));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x, c.y + 1);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, wave));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
					}
					var tmp = front;
					front = nextFront;
					nextFront = tmp;
				} while (nextFront.Count > 0 && result.Count < Keys.Count);
				result.Sort((v1, v2) => v1.length.CompareTo(v2.length));
				if (cachedState == null)
				{
					cachedState = new Dictionary<string, List<(char key, int length)>>(15000);
					reachableCache[o] = cachedState;
				}
				cachedState[PickedUpKeysSorted] = result;
				return result;
			}

			public int FindPathLength()
			{
				var dataCapacity = FreeSpaces * 64;
				var front = new HashSet<(int x, int y, string z)>(dataCapacity);
				var nextFront = new HashSet<(int x, int y, string z)>(dataCapacity);
				var visited = new Dictionary<(int x, int y, string z), int>(dataCapacity);
				foreach (var (x, y) in CurPos)
				{
					front.Add((x, y, ""));
					visited[(x, y, "")] = 0;
				}
				var wave = 0;
				do
				{
					nextFront.Clear();
					wave++;
					foreach (var c in front)
					{
						var nc = (x: c.x - 1, c.y, c.z);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							var k = map[nc.y, nc.x];
							if (IsKey(k) && !nc.z.Contains(k))
							{
								if (nc.z.Length == Keys.Count - 1)
									return wave;

								visited[nc] = wave;
								nc = (nc.x, nc.y, new string((nc.z + k).ToArray().OrderBy(c => c).ToArray()));
							}
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x, c.y - 1, c.z);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							var k = map[nc.y, nc.x];
							if (IsKey(k) && !nc.z.Contains(k))
							{
								if (nc.z.Length == Keys.Count - 1)
									return wave;

								visited[nc] = wave;
								nc = (nc.x, nc.y, new string((nc.z + k).ToArray().OrderBy(c => c).ToArray()));
							}
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x + 1, c.y, c.z);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							var k = map[nc.y, nc.x];
							if (IsKey(k) && !nc.z.Contains(k))
							{
								if (nc.z.Length == Keys.Count - 1)
									return wave;

								visited[nc] = wave;
								nc = (nc.x, nc.y, new string((nc.z + k).ToArray().OrderBy(c => c).ToArray()));
							}
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x, c.y + 1, c.z);
						if (!visited.ContainsKey(nc) && CanMove(nc))
						{
							var k = map[nc.y, nc.x];
							if (IsKey(k) && !nc.z.Contains(k))
							{
								if (nc.z.Length == Keys.Count - 1)
									return wave;

								visited[nc] = wave;
								nc = (nc.x, nc.y, new string((nc.z + k).ToArray().OrderBy(c => c).ToArray()));
							}
							visited[nc] = wave;
							nextFront.Add(nc);
						}
					}
					var tmp = front;
					front = nextFront;
					nextFront = tmp;
					/*
					Console.WriteLine("Wave " + wave);
					Console.WriteLine(ToString(visited, 0));
					*/
				} while (nextFront.Count > 0);
				throw new InvalidOperationException("Shouldn't be here if finish is reachable");
			}

			public List<(char key, List<(int x, int y)>)> FindAllTheoreticallyReachable((int x, int y) o)
			{
				var front = new HashSet<(int x, int y)>(FreeSpaces) {o};
				var nextFront = new HashSet<(int x, int y)>(FreeSpaces);
				var visited = new Dictionary<(int x, int y), int>(FreeSpaces) {[o] = 0};
				int wave = 0;
				var result = new List<(char key, List<(int x, int y)>)>(Keys.Count);
				do
				{
					nextFront.Clear();
					wave++;
					foreach (var c in front)
					{
						var nc = (c.x - 1, c.y);
						if (!visited.ContainsKey(nc) && CanTheoreticallyMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, PathTrace(visited, nc, wave)));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x, c.y - 1);
						if (!visited.ContainsKey(nc) && CanTheoreticallyMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, PathTrace(visited, nc, wave)));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x + 1, c.y);
						if (!visited.ContainsKey(nc) && CanTheoreticallyMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, PathTrace(visited, nc, wave)));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
						nc = (c.x, c.y + 1);
						if (!visited.ContainsKey(nc) && CanTheoreticallyMove(nc))
						{
							ref var s = ref this[nc];
							if (IsKey(s) && !UsedKey(s))
								result.Add((s, PathTrace(visited, nc, wave)));
							visited[nc] = wave;
							nextFront.Add(nc);
						}
					}
					var tmp = front;
					front = nextFront;
					nextFront = tmp;
				} while (nextFront.Count > 0);
				return result;
			}
			private static List<(int x, int y)> PathTrace(Dictionary<(int x, int y), int> waveFronts, (int x, int y) cur, int curWaveFront)
			{
				var result = new List<(int x, int y)>(curWaveFront) {cur};
				while (curWaveFront > 0)
				{
					var nc = (cur.x - 1, cur.y);
					if (waveFronts.TryGetValue(nc, out var nw) && nw == curWaveFront - 1)
					{
						result.Add(nc);
						cur = nc;
						curWaveFront--;
						continue;
					}
					nc = (cur.x, cur.y-1);
					if (waveFronts.TryGetValue(nc, out nw) && nw == curWaveFront - 1)
					{
						result.Add(nc);
						cur = nc;
						curWaveFront--;
						continue;
					}
					nc = (cur.x+1, cur.y);
					if (waveFronts.TryGetValue(nc, out nw) && nw == curWaveFront - 1)
					{
						result.Add(nc);
						cur = nc;
						curWaveFront--;
						continue;
					}
					nc = (cur.x, cur.y+1);
					if (waveFronts.TryGetValue(nc, out nw) && nw == curWaveFront - 1)
					{
						result.Add(nc);
						cur = nc;
						curWaveFront--;
					}
				}
				result.Reverse();
				return result;
			}

			public Dungeon MoveTo(char key) => new Dungeon(this, key);

			public override string ToString() => ToString(-1);

			public string ToString(int moves)
			{
				var keysStat = "Keys: " + PickedUpKeys;
				var result = new StringBuilder(Dimensions.x * Dimensions.y + Dimensions.y * Environment.NewLine.Length + 100)
					.Append(keysStat);
				if (moves > -1)
				{
					var shift = Math.Max(1, Dimensions.x - keysStat.Length);
					result.Append(moves.ToString().PadLeft(shift));
				}
				result.AppendLine();
				for (var y = 0; y < Dimensions.y; y++)
				{
					for (var x = 0; x < Dimensions.x; x++)
					{
						var c = this[(x, y)];
						if (CurPos.Contains((x, y)))
							c = '@';
						else if (IsKey(c) && UsedKey(c))
							c = ' ';
						else if (IsDoor(c) && CanOpen(c))
							c = ' ';
						result.Append(c);
					}
					result.AppendLine();
				}
				return result.ToString();
			}
		}

		private const string TestInput1 = @"
#########
#b A @ a#
#########
";

		private const string TestInput2 = @"
########################
#f D E e C b A @ a B c #
###################### #
#d                     #
########################
";

		private const string TestInput3 = @"
########################
#               b C D f#
# ######################
#     @ a B c d A e F g#
########################
";

		private const string TestInput4 = @"
#################
#i G  c   e  H p#
######## ########
#j A  b   f  D o#
########@########
#k E  a   g  B n#
######## ########
#l F  d   h  C m#
#################
";

		private const string TestInput5 = @"
########################
#@              ac GI b#
###d#e#f################
###A#B#C################
###g#h#i################
########################
";

		private const string TestInput6 = @"
##########
#.a###.Ab#
#.B..@.###
#...######
##########
";

		private const string TestInput7 = @"
#######
#a.#Cd#
##@#@##
#######
##@#@##
#cB#Ab#
#######
";

		private const string TestInput8 = @"
###############
#d.ABC.#.....a#
######@#@######
###############
######@#@######
#b.....#.....c#
###############
";

		private const string TestInput9 = @"
#############
#DcBa.#.GhKl#
#.###@#@#I###
#e#d#####j#k#
###C#@#@###J#
#fEbA.#.FgHi#
#############
";

		private const string TestInput10 = @"
#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba@#@BcIJ#
#############
#nK.L@#@G...#
#M###N#H###.#
#o#m..#i#jk.#
#############
";

		private const string RealInput1 = @"
#################################################################################
#   #             #   # W               # #   #     #         # #       #       #
# # # # ######### # # # ############### # # # # # # # # ##### # # ##### # ### # #
# #   # #       # # # # #       #       #   # # # # # #g#   # #     # #  d  # # #
# ##### # ##### # #D# # # ### ### ####### ### # # # # # # # # ##### # ####### # #
#v  # # # # #   # # #   # #   #   #     #   #   # # # # # # #     #   #   #   # #
### # # # # # ##### ##### ### # ### ### ### ##### # # # # #U##### ##### # # ### #
#   #   # # # #   #   #     # # #     # #   #   # # # # # #   #   #     #   # # #
# ####### # # # # ### # ### #C# ####### # ### # # # # ### ### # ### ######### # #
#   #   # # #   #   # # #   # #  t      # # # #   # #   # #x  # #   #         # #
# # # # # # ####### # # # ### ######### # # # ######### # # ### # ### ####### # #
# # # #   #       # # # #   # #     #   #   #         #   # #  a  #   #  w    # #
### # ##### ### ### # # ### ### ### # ##### ######### ##### # ######### ### ### #
#   #   #   #   #   # # #   #   #   #   #e#     #   #   #   # #  o#     # # #   #
# # ### # ### ### ### # # ### ### ##### # ##### # # ### # ##### # # ##### # # ###
# # # #   #   # F #   # #     # #       # #     # # #   #     X # #   #     # # #
# # # ##### ### ### ####### ### ######### # ##### ### ########### ### ### ### # #
# #     #     # #   # G   #   #   #     # #       #   #         #   #   #   #   #
# # ##### ##### # ### ### ### # # # ### # ####### # ### ##### ##### # # #######T#
# # #     #     # #  u#   # # # # # # # # # L     #     # #   #     # #   #     #
# # # ##### ### # # ### ### # # ### # # # # ############# # ### ######### # #####
# # #     # #   # #   # # #   # #     # #   #             # # #     #j  #       #
# ####### # # ###A##### # # ### # ##### # ### ########### # #Q# ### # # ####### #
#       # # #   #       # # #       #   #   #     #       # # # O #   #     #   #
# # # ### # ############# # ######### # ### # ### ######### # #############J# ###
# # #z#   #               # #   #   # # # # # #   #   #     #     #k  N   # #   #
# # ### ############### ### # # # # # # # # ### ### # # # ####### # ##### # ### #
# #   #   #     #       #   # #   #   # # # #   #   #   # #    q  #   # # # #   #
##### ### # ### # ####### ### ########### # # ### ### ##### ####### # # # # # ###
#       # # # # #         #             # # #   # #   #   # #       # #n  # # #c#
# # ##### # # # ############### ####### # # # # # # ### # # ######### # ### # # #
# # #   #   # #     #         # #   #   # # # # # #   # # #       K   #     #   #
# ### # ##### ##### ##### ### # # # # # # # ### # # ### # ##################### #
# #   #   #       #       #   #   # # # # # #   # # #   #                 #     #
# # ### ###Y# ############# ####### # # # # # ### ### ####### ######### ### #####
# # # #     # #           # #     # # # # # # #   #   #     # #   #   # #   #   #
# # # ####### # ### ##### # # # # ### # # # # # # # ### ### # # # # # ### ### # #
#   #       #   # #   #   # # # #   # # #   # # # # #     # # # #   #   #     #i#
#M### ##### ##### ### ##### ### ### # # # ### # ### ####### # # ####### ####### #
#    b#             #           #     #       #             #         #         #
####################################### @ #######################################
# #       #   #       #       #     #     #   #               #     B     #     #
# # # ### # ### # ### # ##### # ### # # # # # # ##### ##### ### # ######### ### #
#   # #     #   # # # # #   # # #   # # #   # # # #   #     #   # #     #   # # #
#H### # ##### ### # # # ### # # # ### # # ### # # # # ####### ### # ### # ### # #
# #   #   #   # # # # # #   #   #     # #   #   # # # #   #   #   # # # # #   # #
# # ##### # ### # # # # # ############# ### ##### # # # # # ### ### # # # # # # #
# #     # #   # # #   # #     #     #   #   #     # # # # # # #   #   #   # # # #
# ##### # ### # # ### # # # ### ### # # # ####### # ### # # # ### ### ##### # # #
#     # # # # # #   # # # #   #   # # # #         #     # #     # #   #     # #f#
####### # # # # ### ### ##### # ### # ########### ####### # ##### # ##### # # # #
#       #   #     #     #   #   #   #  l#       # #       # #     # #   # # # # #
# ####### ### ##### ##### # ##### # ### # # ##### # ##### # # ##### # # ### ### #
#   # I # #   #   #       #       # #   # #       #   #  y# #   Z # # #   # #   #
### # ### # ### # ############# ##### # # ########### ##### ##### # #V### # # # #
#   #     # #   #   #           #     # #   E       #     #   # #   # #     # # #
# ####### # # ##### ##### ####### ################# ##### ### # ##### # ##### ###
#   P   # # # #   #     # #     #   #   #         #   # #     #  m  # # #   #   #
# ##### ### # # # ##### ### ### # # # # # # ######### # ####### ### # #R# # ### #
# #     #   #   # #       # # # # #   # # #   #     #     #     #   # # # #   # #
# # ##### ####### ####### # # # ####### # ### # ### ##### # ##### ### # # ### # #
# #     #     # #   # #     #   #       #   # # # #     # # # #   #   # # # #   #
####### ##### # ### # ####### ### ######### # # # ##### # # # # ### ### # # ### #
#         # #   #   #     # #   #       #   # #   #     #   # # #   #   # #     #
# ####### # ### # # ##### # ### # ##### # ### ### # ##### ### # ### # ### ### ###
#   #         # # #   #   # #   #   # # # # #   # # #     #   # #   #   #   #  p#
### # ######### # ##### ### # ##### # # # # ### # # # ##### ### # ##### ### ### #
#   # #         #       # #   #       # # #   # # # # # #   # # # #   # # #   # #
# ### # ################# # ######### # # ### # # # # # # ### # # ### # # ### # #
#   # # #       #   #     # #       # # # #   #   # # # #   #   #   #     # # # #
# # ### # ### # # # ##### # ### ### # # # # ####### # # ### # ### # ##### # # ###
# #   # # #   # # #   #   #   #   # # # #   #     # #   #   # #   #   #     #   #
##### # ### ### # ### # # ### ### # ### # ### ### # ### # ### ####### # ####### #
#  r# #   # #     #   #s#   #     #   # # #   # # # #   # #   #       # #   S   #
# # # ### # ####### ### ############# # ### ### # # # ### # ### ####### # ##### #
# #     # # #   # #   #         #   # # #   #       #   # #     # #     # # #   #
# ####### # # # # ### # ### ### # # # # # ############# # ####### # ##### # # ###
# #     # #   # #     #   # #   # # #   #     #   #   # #     #     #  h  # # # #
# # ### # ##### # ######### # ### # ##### ### # # # # # ##### ####### ##### # # #
#     #         #           #     #     #   #   #   #       #         #         #
#################################################################################
";

		private const string RealInput2 = @"
#################################################################################
#   #             #   # W               # #   #     #         # #       #       #
# # # # ######### # # # ############### # # # # # # # # ##### # # ##### # ### # #
# #   # #       # # # # #       #       #   # # # # # #g#   # #     # #  d  # # #
# ##### # ##### # #D# # # ### ### ####### ### # # # # # # # # ##### # ####### # #
#v  # # # # #   # # #   # #   #   #     #   #   # # # # # # #     #   #   #   # #
### # # # # # ##### ##### ### # ### ### ### ##### # # # # #U##### ##### # # ### #
#   #   # # # #   #   #     # # #     # #   #   # # # # # #   #   #     #   # # #
# ####### # # # # ### # ### #C# ####### # ### # # # # ### ### # ### ######### # #
#   #   # # #   #   # # #   # #  t      # # # #   # #   # #x  # #   #         # #
# # # # # # ####### # # # ### ######### # # # ######### # # ### # ### ####### # #
# # # #   #       # # # #   # #     #   #   #         #   # #  a  #   #  w    # #
### # ##### ### ### # # ### ### ### # ##### ######### ##### # ######### ### ### #
#   #   #   #   #   # # #   #   #   #   #e#     #   #   #   # #  o#     # # #   #
# # ### # ### ### ### # # ### ### ##### # ##### # # ### # ##### # # ##### # # ###
# # # #   #   # F #   # #     # #       # #     # # #   #     X # #   #     # # #
# # # ##### ### ### ####### ### ######### # ##### ### ########### ### ### ### # #
# #     #     # #   # G   #   #   #     # #       #   #         #   #   #   #   #
# # ##### ##### # ### ### ### # # # ### # ####### # ### ##### ##### # # #######T#
# # #     #     # #  u#   # # # # # # # # # L     #     # #   #     # #   #     #
# # # ##### ### # # ### ### # # ### # # # # ############# # ### ######### # #####
# # #     # #   # #   # # #   # #     # #   #             # # #     #j  #       #
# ####### # # ###A##### # # ### # ##### # ### ########### # #Q# ### # # ####### #
#       # # #   #       # # #       #   #   #     #       # # # O #   #     #   #
# # # ### # ############# # ######### # ### # ### ######### # #############J# ###
# # #z#   #               # #   #   # # # # # #   #   #     #     #k  N   # #   #
# # ### ############### ### # # # # # # # # ### ### # # # ####### # ##### # ### #
# #   #   #     #       #   # #   #   # # # #   #   #   # #    q  #   # # # #   #
##### ### # ### # ####### ### ########### # # ### ### ##### ####### # # # # # ###
#       # # # # #         #             # # #   # #   #   # #       # #n  # # #c#
# # ##### # # # ############### ####### # # # # # # ### # # ######### # ### # # #
# # #   #   # #     #         # #   #   # # # # # #   # # #       K   #     #   #
# ### # ##### ##### ##### ### # # # # # # # ### # # ### # ##################### #
# #   #   #       #       #   #   # # # # # #   # # #   #                 #     #
# # ### ###Y# ############# ####### # # # # # ### ### ####### ######### ### #####
# # # #     # #           # #     # # # # # # #   #   #     # #   #   # #   #   #
# # # ####### # ### ##### # # # # ### # # # # # # # ### ### # # # # # ### ### # #
#   #       #   # #   #   # # # #   # # #   # # # # #     # # # #   #   #     #i#
#M### ##### ##### ### ##### ### ### # # # ### # ### ####### # # ####### ####### #
#    b#             #           #     #@#@    #             #         #         #
#################################################################################
# #       #   #       #       #     #  @#@#   #               #     B     #     #
# # # ### # ### # ### # ##### # ### # # # # # # ##### ##### ### # ######### ### #
#   # #     #   # # # # #   # # #   # # #   # # # #   #     #   # #     #   # # #
#H### # ##### ### # # # ### # # # ### # # ### # # # # ####### ### # ### # ### # #
# #   #   #   # # # # # #   #   #     # #   #   # # # #   #   #   # # # # #   # #
# # ##### # ### # # # # # ############# ### ##### # # # # # ### ### # # # # # # #
# #     # #   # # #   # #     #     #   #   #     # # # # # # #   #   #   # # # #
# ##### # ### # # ### # # # ### ### # # # ####### # ### # # # ### ### ##### # # #
#     # # # # # #   # # # #   #   # # # #         #     # #     # #   #     # #f#
####### # # # # ### ### ##### # ### # ########### ####### # ##### # ##### # # # #
#       #   #     #     #   #   #   #  l#       # #       # #     # #   # # # # #
# ####### ### ##### ##### # ##### # ### # # ##### # ##### # # ##### # # ### ### #
#   # I # #   #   #       #       # #   # #       #   #  y# #   Z # # #   # #   #
### # ### # ### # ############# ##### # # ########### ##### ##### # #V### # # # #
#   #     # #   #   #           #     # #   E       #     #   # #   # #     # # #
# ####### # # ##### ##### ####### ################# ##### ### # ##### # ##### ###
#   P   # # # #   #     # #     #   #   #         #   # #     #  m  # # #   #   #
# ##### ### # # # ##### ### ### # # # # # # ######### # ####### ### # #R# # ### #
# #     #   #   # #       # # # # #   # # #   #     #     #     #   # # # #   # #
# # ##### ####### ####### # # # ####### # ### # ### ##### # ##### ### # # ### # #
# #     #     # #   # #     #   #       #   # # # #     # # # #   #   # # # #   #
####### ##### # ### # ####### ### ######### # # # ##### # # # # ### ### # # ### #
#         # #   #   #     # #   #       #   # #   #     #   # # #   #   # #     #
# ####### # ### # # ##### # ### # ##### # ### ### # ##### ### # ### # ### ### ###
#   #         # # #   #   # #   #   # # # # #   # # #     #   # #   #   #   #  p#
### # ######### # ##### ### # ##### # # # # ### # # # ##### ### # ##### ### ### #
#   # #         #       # #   #       # # #   # # # # # #   # # # #   # # #   # #
# ### # ################# # ######### # # ### # # # # # # ### # # ### # # ### # #
#   # # #       #   #     # #       # # # #   #   # # # #   #   #   #     # # # #
# # ### # ### # # # ##### # ### ### # # # # ####### # # ### # ### # ##### # # ###
# #   # # #   # # #   #   #   #   # # # #   #     # #   #   # #   #   #     #   #
##### # ### ### # ### # # ### ### # ### # ### ### # ### # ### ####### # ####### #
#  r# #   # #     #   #s#   #     #   # # #   # # # #   # #   #       # #   S   #
# # # ### # ####### ### ############# # ### ### # # # ### # ### ####### # ##### #
# #     # # #   # #   #         #   # # #   #       #   # #     # #     # # #   #
# ####### # # # # ### # ### ### # # # # # ############# # ####### # ##### # # ###
# #     # #   # #     #   # #   # # #   #     #   #   # #     #     #  h  # # # #
# # ### # ##### # ######### # ### # ##### ### # # # # # ##### ####### ##### # # #
#     #         #           #     #     #   #   #   #       #         #         #
#################################################################################
";
	}
}