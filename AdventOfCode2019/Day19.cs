﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2019
{
    [TestFixture(TestName = "Day 19: Tractor Beam")]
    public class Day19
    {
        [TestCase(RealInput, ExpectedResult = 158)]
        public int Part1(string input)
        {
            const int maxX = 50;
            const int maxY = 50;
            var ovm = new IntcodeVM(input);
            var inputValues = new Queue<long>(2);
            var output = new List<bool>(50 * 50);
            for (var y = 0; y <maxY; y++)
            {
                for (var x = 0; x < maxX; x++)
                {
                    var r = IsHit(ovm, x, y, inputValues);
                    Console.Write(r ? "#" : ".");
                    output.Add(r);
                }
                Console.WriteLine();
            }
            Assert.That(output.Count, Is.EqualTo(50*50));
            return output.Count(v => v);
        }

        [TestCase(RealInput, ExpectedResult = 6191165)]
        public int Part2(string input)
        {
            var ovm = new IntcodeVM(input);
            var queue = new Queue<long>(2);
            var prevX = 619;
            var testX = prevX;
            int fitResult;
            do
            {
                fitResult = TestFit(ovm, testX, queue);
                if (fitResult == -1)
                {
                    if (prevX > testX)
                    {
                        var tmp = testX;
                        testX += (prevX - testX) / 2;
                        prevX = tmp;
                    }
                    else
                    {
                        prevX = testX;
                        testX *= 2;
                    }
                }
                else if (fitResult == 1)
                {
                    if (prevX < testX)
                    {
                        var tmp = testX;
                        testX -= (testX - prevX) / 2;
                        prevX = tmp;
                    }
                    else
                    {
                        prevX = testX;
                        testX /= 2;
                    }
                }
                else
                {
                    if (TestFit(ovm, testX - 1, queue) == 0)
                    {
                        fitResult = 1;
                        prevX = testX;
                        testX--;
                    }
                }
            } while (fitResult != 0);
            var (x, y) = (testX - 99, FindY(ovm, testX, queue));
            Console.WriteLine($"{x}, {y}");
            /*
            for (var vy = y-50; vy<y+100+50; vy++)
            {
                for (var vx = x - 50; vx < x + 100 + 50; vx++)
                {
                    var v = IsHit(ovm, vx, vy, queue);
                    if (   x <= vx && vx < x + 100
                        && y <= vy && vy < y + 100)
                    {
                        Console.Write(v ? 'O' : 'X');
                    }
                    else
                    {
                        Console.Write(v ? '.' : ' ');
                    }
                }
                Console.WriteLine();
            }
            */
            return x * 10000 + y;
        }


        private static int TestFit(IntcodeVM ovm, int testX, Queue<long> inputValues)
        {
            if (testX < 100)
                return -1;

            var testY = FindY(ovm, testX, inputValues);
            if (!IsHit(ovm, testX - 99, testY, inputValues))
                return -1;

            if (!IsHit(ovm, testX - 99, testY + 99, inputValues))
                return -1;

            if (IsHit(ovm, testX - 100, testY + 99, inputValues))
                return 1;

            return 0;
        }

        private static int FindY(IntcodeVM ovm, int testX, Queue<long> inputValues)
        {
            var y = 0;
            while (!IsHit(ovm, testX, y, inputValues))
                y++;
            return y;
        }

        private static bool IsHit(IntcodeVM ovm, long x, long y, Queue<long> inputValues)
        {
            inputValues.Enqueue(x);
            inputValues.Enqueue(y);
            bool result = false;
            var vm = new IntcodeVM(ovm.GetProgramState())
            {
                OnDataIn = vm => inputValues.Dequeue(),
                OnDataOut = v => result = v == 1,
            };
            vm.Execute();
            return result;
        }

        private static IEnumerable<long> GenRobotCoordsXY(int maxX, int maxY)
        {
            for (var y = 0; y < maxY; y++)
            for (var x = 0; x < maxX; x++)
            {
                yield return x;
                yield return y;
            }
        }

        private const string RealInput = @"
109,424,203,1,21101,0,11,0,1105,1,282,21101,0,18,0,1106,0,259,2102,1,1,221,203,1,21102,31,1,0,1105,1,282,21101,38,0,0,1105,1,259,21001,23,0,2,21201,1,0,3,21101,0,1,1,21101,0,57,0,1106,0,303,1202,1,1,222,20102,1,221,3,21002,221,1,2,21101,259,0,1,21102,80,1,0,1106,0,225,21101,0,189,2,21102,91,1,0,1105,1,303,2102,1,1,223,20101,0,222,4,21102,259,1,3,21101,225,0,2,21102,225,1,1,21102,1,118,0,1105,1,225,21001,222,0,3,21102,1,57,2,21102,1,133,0,1106,0,303,21202,1,-1,1,22001,223,1,1,21102,148,1,0,1106,0,259,1202,1,1,223,21001,221,0,4,20101,0,222,3,21101,0,24,2,1001,132,-2,224,1002,224,2,224,1001,224,3,224,1002,132,-1,132,1,224,132,224,21001,224,1,1,21101,195,0,0,106,0,108,20207,1,223,2,20102,1,23,1,21102,-1,1,3,21101,0,214,0,1106,0,303,22101,1,1,1,204,1,99,0,0,0,0,109,5,1201,-4,0,249,22101,0,-3,1,22101,0,-2,2,22102,1,-1,3,21102,250,1,0,1106,0,225,22101,0,1,-4,109,-5,2106,0,0,109,3,22107,0,-2,-1,21202,-1,2,-1,21201,-1,-1,-1,22202,-1,-2,-2,109,-3,2106,0,0,109,3,21207,-2,0,-1,1206,-1,294,104,0,99,21201,-2,0,-2,109,-3,2105,1,0,109,5,22207,-3,-4,-1,1206,-1,346,22201,-4,-3,-4,21202,-3,-1,-1,22201,-4,-1,2,21202,2,-1,-1,22201,-4,-1,1,21201,-2,0,3,21102,343,1,0,1105,1,303,1105,1,415,22207,-2,-3,-1,1206,-1,387,22201,-3,-2,-3,21202,-2,-1,-1,22201,-3,-1,3,21202,3,-1,-1,22201,-3,-1,2,21201,-4,0,1,21101,384,0,0,1106,0,303,1106,0,415,21202,-4,-1,-4,22201,-4,-3,-4,22202,-3,-2,-2,22202,-2,-4,-4,22202,-3,-2,-3,21202,-4,-1,-2,22201,-3,-2,1,22102,1,1,-4,109,-5,2105,1,0
";
    }
}