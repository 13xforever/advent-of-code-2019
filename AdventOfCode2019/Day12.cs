﻿using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 12: The N-Body Problem")]
	public class Day12
	{
		[TestCase(TestInput1, 10, ExpectedResult = 179)]
		[TestCase(TestInput2, 100, ExpectedResult = 1940)]
		[TestCase(RealInput, 1000, ExpectedResult = 7179)]
		public int Part1(string input, int steps)
		{
			var result = Sim(input, steps);
			return result.Sum(b => b.PotentialEnergy * b.KineticEnergy);
		}

		[TestCase(TestInput1, ExpectedResult = 2772)]
		[TestCase(TestInput2, ExpectedResult = 4686774924)]
		[TestCase(RealInput, ExpectedResult = 428576638953552)]
		public long Part2(string input)
		{
			var bodies = ParseInput(input);
			return SimFullCycle(bodies);
		}

		private static long SimFullCycle(Body[] bodies)
		{
			static (int, int) GetXHash(Body[] b) => (
				HashCode.Combine(b[0].Position.X.GetHashCode(), b[1].Position.X.GetHashCode(), b[2].Position.X.GetHashCode(), b[3].Position.X.GetHashCode()),
				HashCode.Combine(b[0].Velocity.X.GetHashCode(), b[1].Velocity.X.GetHashCode(), b[2].Velocity.X.GetHashCode(), b[3].Velocity.X.GetHashCode())
			);
			static (int, int) GetYHash(Body[] b) => (
				HashCode.Combine(b[0].Position.Y.GetHashCode(), b[1].Position.Y.GetHashCode(), b[2].Position.Y.GetHashCode(), b[3].Position.Y.GetHashCode()),
				HashCode.Combine(b[0].Velocity.Y.GetHashCode(), b[1].Velocity.Y.GetHashCode(), b[2].Velocity.Y.GetHashCode(), b[3].Velocity.Y.GetHashCode())
			);
			static (int, int) GetZHash(Body[] b) => (
				HashCode.Combine(b[0].Position.Z.GetHashCode(), b[1].Position.Z.GetHashCode(), b[2].Position.Z.GetHashCode(), b[3].Position.Z.GetHashCode()),
				HashCode.Combine(b[0].Velocity.Z.GetHashCode(), b[1].Velocity.Z.GetHashCode(), b[2].Velocity.Z.GetHashCode(), b[3].Velocity.Z.GetHashCode())
			);
			var steps = 0;
			var xStartHash = GetXHash(bodies);
			var yStartHash = GetYHash(bodies);
			var zStartHash = GetZHash(bodies);
			int xCycle = 0, yCycle = 0, zCycle = 0;
			do
			{
				steps++;
				SimStep(bodies);
				if (xCycle == 0 && GetXHash(bodies) == xStartHash) xCycle = steps;
				if (yCycle == 0 && GetYHash(bodies) == yStartHash) yCycle = steps;
				if (zCycle == 0 && GetZHash(bodies) == zStartHash) zCycle = steps;
			} while (xCycle == 0 || yCycle == 0 || zCycle == 0);
			return Lcm(xCycle, yCycle, zCycle);
		}

		private static long Lcm(long a, long b) => a * b / Gcd(a, b);
		private static long Lcm(int a, int b, int c) => Lcm(Lcm(a, b), c);

		private static long Gcd(long a, long b)
		{
			while (a != b)
			{
				if (a % b == 0)
					return b;

				if (b % a == 0)
					return a;

				if (a > b)
					a -= b;
				if (b > a)
					b -= a;
			}
			return a;
		}

		private static Body[] ParseInput(string input)
			=> input
				.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
				.Select(l => new Body { Position = Point.Parse(l) })
				.ToArray();

		private Body[] Sim(string input, int steps)
		{
			var bodies = ParseInput(input);
			for (var s = 0; s < steps; s++)
				SimStep(bodies);
			return bodies;
		}

		private static void SimStep(Body[] bodies)
		{
			for (var i1 = 0; i1 < bodies.Length - 1; i1++)
			for (var i2 = i1 + 1; i2 < bodies.Length; i2++)
			{
				ref var b1 = ref bodies[i1];
				ref var b2 = ref bodies[i2];
				var diff = b1.Position - b2.Position;
				diff = diff.Normalize();
				b1.Velocity -= diff;
				b2.Velocity += diff;
			}
			for (var i = 0; i < bodies.Length; i++)
				bodies[i].Position += bodies[i].Velocity;
		}

		[DebuggerDisplay("({X}, {Y}, {Z})")]
		private struct Point
		{
			public Point(int x, int y, int z)
			{
				X = x;
				Y = y;
				Z = z;
			}

			public int X;
			public int Y;
			public int Z;

			public int Energy => Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z);
			public Point Abs() => new Point(Math.Abs(X), Math.Abs(Y), Math.Abs(Z));
			public Point Normalize() => new Point(X < 0 ? -1 : X == 0 ? 0 : 1, Y < 0 ? -1 : Y == 0 ? 0 : 1, Z < 0 ? -1 : Z == 0 ? 0 : 1);

			public static Point Parse(string s)
			{
				var parts = s
					.TrimStart('<')
					.TrimEnd('>')
					.Split(',', StringSplitOptions.RemoveEmptyEntries)
					.Select(p => int.Parse(p.Split('=')[1]))
					.ToArray();
				return new Point(parts[0], parts[1], parts[2]);
			}

			public static implicit operator Point((int x, int y, int z) p) => new Point(p.x, p.y, p.z);
			public static implicit operator (int x, int y, int z)(Point p) => (p.X, p.Y, p.Z);

			public static Point operator +(Point p1, Point p2) => new Point(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z);
			public static Point operator -(Point p1, Point p2) => new Point(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
			public static Point operator *(Point p1, Point p2) => new Point(p1.X * p2.X, p1.Y * p2.Y, p1.Z * p2.Z);
			public static Point operator /(Point p1, Point p2) => new Point(p1.X / p2.X, p1.Y / p2.Y, p1.Z / p2.Z);
			public static bool operator ==(Point left, Point right) => left.Equals(right);
			public static bool operator !=(Point left, Point right) => !left.Equals(right);

			public override bool Equals(object? obj) => obj is Point other && Equals(other);
			public bool Equals(Point other) => X == other.X && Y == other.Y && Z == other.Z;
			public override int GetHashCode() => HashCode.Combine(X, Y, Z);

			public override string ToString() => $"{X}, {Y}, {Z}";
		}

		[DebuggerDisplay("{Position} {Velocity}")]
		private struct Body
		{
			public Point Position;
			public Point Velocity;

			public int PotentialEnergy => Position.Energy;
			public int KineticEnergy => Velocity.Energy;

			public override bool Equals(object? obj) => base.Equals(obj);
			public bool Equals(Body other) => Position.Equals(other.Position) && Velocity.Equals(other.Velocity);
			public override int GetHashCode() => HashCode.Combine(Position, Velocity);

			public static bool operator ==(Body left, Body right) { return left.Equals(right); }
			public static bool operator !=(Body left, Body right) { return !left.Equals(right); }

			public override string ToString() => $"({Position}) <{Velocity}>";
		}

		private const string TestInput1 = @"
<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>
";

		private const string TestInput2 = @"
<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>
";

		private const string RealInput = @"
<x=3, y=15, z=8>
<x=5, y=-1, z=-2>
<x=-10, y=8, z=2>
<x=8, y=4, z=-5>
";
	}
}