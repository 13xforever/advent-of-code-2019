﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 17: Set and Forget")]
	public class Day17
	{
		[TestCase("R11111111R11111111R1111R1111R11111111L111111L11R1111R1111R11111111R11111111R11111111L111111L11")]
		public void Intermediate2(string directionPath)
		{
			var results = BytePairEncoding(directionPath);
			Console.WriteLine(string.Join(Environment.NewLine, results ?? new string[0]));
			Assert.That(results, Is.Not.Null);
		}

		[TestCase(RealInput, ExpectedResult = 9876)]
		public int Part1(string input)
		{
			var stringBuilder = new StringBuilder();
			var vm = new IntcodeVM(input)
			{
				OnDataOut = c => stringBuilder.Append((char)c)
			};
			vm.Execute();
			var map = CameraView.Build(stringBuilder);
			Console.WriteLine(stringBuilder);
			var result = 0;
			var intersectionList = map.field.Where(c => c.Value == '#' && map.IsIntersection(c.Key)).Select(c => c.Key).ToList();
			foreach (var cell in intersectionList)
				result += map.GetAlignment(cell);
			return result;
		}

		[TestCase(RealInput, ExpectedResult = 1234055)]
		public long Part2(string input)
		{
			var ovm = new IntcodeVM(input);
			var cameraFeed = new StringBuilder();
			var vm = new IntcodeVM(ovm.GetProgramState())
			{
				OnDataOut = c => cameraFeed.Append((char)c)
			};
			vm.Execute();
			var map = CameraView.Build(cameraFeed);

			var pathLenght = map.field.Values.Count(c => c == '#');
			var plan = (
				from coordPath in BuildPaths(map, pathLenght)
				let dirPath = BuildCompleteInstructions(coordPath, map.CurDir)
				let solution = BytePairEncoding(dirPath)
				where solution?.All(l => l.Length < 21) ?? false
				select solution
			).First();
			Assert.That(plan, Is.Not.Null);
			Console.WriteLine(string.Join(Environment.NewLine, plan));

			var inputData = new Queue<long>();
			foreach (var l in plan)
			{
				foreach (var c in l)
					inputData.Enqueue(c);
				inputData.Enqueue('\n');
			}
			inputData.Enqueue('n');
			inputData.Enqueue('\n');

			var result = -1L;
			vm = new IntcodeVM(ovm.GetProgramState())
			{
				[0] = 2,
				OnDataIn = _ => inputData.Dequeue(),
				OnDataOut = r => result = r,
			};
			vm.Execute();
			return result;
		}

		private IEnumerable<ImmutableList<(int x, int y)>> BuildPaths(CameraView map, int pathLength, ImmutableHashSet<(int x, int y)> visited = null, ImmutableList<(int x, int y)> path = null)
		{
			static bool hasUnvisited(CameraView map, ImmutableHashSet<(int x, int y)> visited, (int x, int y) c)
			{
				var t = (c.x + 1, c.y);
				if (map[t] == '#' && !visited.Contains(t))
					return true;
				t = (c.x - 1, c.y);
				if (map[t] == '#' && !visited.Contains(t))
					return true;
				t = (c.x, c.y + 1);
				if (map[t] == '#' && !visited.Contains(t))
					return true;
				t = (c.x, c.y - 1);
				if (map[t] == '#' && !visited.Contains(t))
					return true;
				return false;
			}
			visited ??= ImmutableHashSet<(int x, int y)>.Empty.Add(map.CurPos);
			path ??= ImmutableList<(int x, int y)>.Empty.Add(map.CurPos);
			var (ox, oy) = path[^1];
			var nxt = (ox + 1, oy);
			if (map[nxt] == '#'
				&& (visited.Count < 2 || nxt != path[^2])
				&& (!visited.Contains(nxt) || hasUnvisited(map, visited.Add(nxt), nxt)))
			{
				var nxtVisited = visited.Add(nxt);
				if (nxtVisited.Count == pathLength)
					yield return path.Add(nxt);

				foreach (var p in BuildPaths(map, pathLength, nxtVisited, path.Add(nxt)))
					yield return p;
			}
			nxt = (ox - 1, oy);
			if (map[nxt] == '#'
				&& (visited.Count < 2 || nxt != path[^2])
				&& (!visited.Contains(nxt) || hasUnvisited(map, visited.Add(nxt), nxt)))
			{
				var nxtVisited = visited.Add(nxt);
				if (nxtVisited.Count == pathLength)
					yield return path.Add(nxt);

				foreach (var p in BuildPaths(map, pathLength, nxtVisited, path.Add(nxt)))
					yield return p;
			}
			nxt = (ox, oy + 1);
			if (map[nxt] == '#'
				&& (visited.Count < 2 || nxt != path[^2])
				&& (!visited.Contains(nxt) || hasUnvisited(map, visited.Add(nxt), nxt)))
			{
				var nxtVisited = visited.Add(nxt);
				if (nxtVisited.Count == pathLength)
					yield return path.Add(nxt);

				foreach (var p in BuildPaths(map, pathLength, nxtVisited, path.Add(nxt)))
					yield return p;
			}
			nxt = (ox, oy - 1);
			if (map[nxt] == '#'
				&& (visited.Count < 2 || nxt != path[^2])
				&& (!visited.Contains(nxt) || hasUnvisited(map, visited.Add(nxt), nxt)))
			{
				var nxtVisited = visited.Add(nxt);
				if (nxtVisited.Count == pathLength)
					yield return path.Add(nxt);

				foreach (var p in BuildPaths(map, pathLength, nxtVisited, path.Add(nxt)))
					yield return p;
			}
		}

		private string BuildCompleteInstructions(ImmutableList<(int x, int y)> path, CameraView.Dir dir)
		{
			var buf = ArrayPool<char>.Shared.Rent(path.Count * 2);
			var j = 0;
			for (var i = 0; i < path.Count-1; i++)
			{
				var p = path.ItemRef(i);
				var np = path.ItemRef(i+1);
				var desiredDir = GetDesiredDir(p, np);
				if (desiredDir == dir)
					buf[j] = '1';
				else
				{
					buf[j] = GetRotation(dir, desiredDir);
					j++;
					dir = desiredDir;
					buf[j] = '1';
				}
				j++;
			}
			var result = new string(buf, 0, j);
			ArrayPool<char>.Shared.Return(buf);
			return result;
		}

		private static CameraView.Dir GetDesiredDir((int x, int y) cur, (int x, int y) nxt)
			=> cur.x == nxt.x
				? cur.y > nxt.y // y grows down
					? CameraView.Dir.U
					: CameraView.Dir.D
				: cur.x < nxt.x // x grows right
					? CameraView.Dir.R
					: CameraView.Dir.L;

		private static char GetRotation(CameraView.Dir cur, CameraView.Dir desired) => CameraView.TurnLeft(cur) == desired ? 'L' : 'R';

		private static string CompressPath(string dirPath)
		{
			var n = 0;
			var result = new StringBuilder(dirPath.Length);
			foreach (var c in dirPath)
			{
				switch (c)
				{
					case '1':
					{
						n++;
						break;
					}
					default:
					{
						if (n > 0)
						{
							result.Append(n.ToString()).Append(',');
							n = 0;
						}
						result.Append(c).Append(',');
						break;
					}
				}
			}
			if (n > 0)
				return result.Append(n.ToString()).ToString();
			return result.ToString(0, result.Length - 1);
		}

		private static string[] BytePairEncoding(string dirPath)
		{
			var subs = new Dictionary<char, string>();
			var freqs = new Dictionary<string, int>();
			var curSub = 'Z';
			do
			{
				freqs.Clear();
				for (var i = 0; i < dirPath.Length - 1; i++)
				{
					var word = dirPath[i..(i + 2)];
					freqs.TryGetValue(word, out var stat);
					freqs[word] = stat + 1;
				}
				var longestWord = freqs.OrderByDescending(s => s.Value).First();
				if (longestWord.Value > 1)
				{
					subs[curSub] = longestWord.Key;
					dirPath = dirPath.Replace(longestWord.Key, curSub.ToString());
					do
					{
						curSub = (char)(curSub - 1);
					} while (curSub == 'R' || curSub == 'L' || curSub == '1');
				}
				else
					break;
			} while (dirPath.Length > 10 || dirPath.Distinct().Count() > 3);
			//Console.WriteLine(dirPath);

			var finalChars = dirPath.Distinct().ToArray();
			if (finalChars.Length > 3)
				return null;

			var topCmd = dirPath
				.Replace(finalChars[0], 'A')
				.Replace(finalChars[1], 'B')
				.Replace(finalChars[2], 'C');

			string inflate(char word)
			{
				var result = word.ToString();
				bool updated;
				do
				{
					updated = false;
					var unique = result.Distinct();
					foreach (var c in unique)
					{
						if (subs.TryGetValue(c, out var sub))
						{
							result = result.Replace(c.ToString(), sub);
							updated = true;
						}
					}
				} while (updated);
				return result;
			}

			return new[] {CompressPath(topCmd)}.Concat(finalChars.Select(inflate).Select(CompressPath)).ToArray();
		}

		private class CameraView: SparseField<char>
		{
			public CameraView()
			{
				SpriteMap = c => c switch
				{
					'\0' => '・',
					'.' => '・',
					'#' => '＃',
					'<' => '《',
					'>' => '》',
					'^' => '︽',
					'v' => '︾',
					'X' => '㊥',
					_ => c,
				};
			}

			public static CameraView Build(StringBuilder cameraOutput)
			{
				var result = new CameraView();
				var lines = cameraOutput.ToString().Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
				for (var l = 0; l < lines.Length; l++)
				{
					var line = lines[l];
					for (var i = 0; i < line.Length; i++)
					{
						var c = line[i];
						switch (c)
						{
							case '.':
							{
								break;
							}
							case '#':
							{
								result[(i, l)] = c;
								break;
							}
							case '^':
							case '<':
							case '>':
							case 'v':
							case 'X':
							{
								result.CurPos = (i, l);
								if (c == 'X')
									result[(i, l)] = '.';
								else
								{
									result[(i, l)] = '#';
									result.CurDir = c switch
									{
										'^' => Dir.U,
										'<' => Dir.L,
										'>' => Dir.R,
										'v' => Dir.D,
										_ => throw new DataException("Invalid robot orientation " + c),
									};
								}
								break;
							}
							default:
								throw new DataException("Unknown map character " + c);
						}
					}
				}
				return result;
			}

			public bool IsIntersection(int x, int y)
				=> this[(x, y)] == '#'
				   && this[(x - 1, y)] == '#'
				   && this[(x + 1, y)] == '#'
				   && this[(x, y - 1)] == '#'
				   && this[(x, y + 1)] == '#';

			public bool IsIntersection((int x, int y) coord) => IsIntersection(coord.x, coord.y);

			public int GetAlignment(int x, int y) => x * y;

			public int GetAlignment((int x, int y) coord) => GetAlignment(coord.x, coord.y);

			public override string ToString()
			{
				var (minX, maxX, minY, maxY) = GetDimensions();
				var result = new StringBuilder();
				for (var y = minY; y <= maxY; y++)
				{
					for (var x = minX; x <= maxX; x++)
					{
						if ((x, y) == CurPos)
							result.Append(SpriteMap('X'));
						else if (IsIntersection(x, y))
							result.Append('〇');
						else
							result.Append(SpriteMap(this[(x, y)]));
					}
					result.AppendLine();
				}
				return result.ToString();
			}
		}

		private const string RealInput = @"
1,330,331,332,109,4288,1102,1,1182,15,1102,1479,1,24,1002,0,1,570,1006,570,36,1002,571,1,0,1001,570,-1,570,1001,24,1,24,1105,1,18,1008,571,0,571,1001,15,1,15,1008,15,1479,570,1006,570,14,21102,58,1,0,1106,0,786,1006,332,62,99,21101,0,333,1,21101,73,0,0,1106,0,579,1102,1,0,572,1102,0,1,573,3,574,101,1,573,573,1007,574,65,570,1005,570,151,107,67,574,570,1005,570,151,1001,574,-64,574,1002,574,-1,574,1001,572,1,572,1007,572,11,570,1006,570,165,101,1182,572,127,1001,574,0,0,3,574,101,1,573,573,1008,574,10,570,1005,570,189,1008,574,44,570,1006,570,158,1106,0,81,21101,340,0,1,1105,1,177,21102,477,1,1,1106,0,177,21101,514,0,1,21101,0,176,0,1105,1,579,99,21102,184,1,0,1105,1,579,4,574,104,10,99,1007,573,22,570,1006,570,165,1002,572,1,1182,21101,0,375,1,21102,1,211,0,1106,0,579,21101,1182,11,1,21101,0,222,0,1106,0,979,21101,388,0,1,21101,233,0,0,1105,1,579,21101,1182,22,1,21101,0,244,0,1105,1,979,21102,401,1,1,21102,255,1,0,1105,1,579,21101,1182,33,1,21101,0,266,0,1106,0,979,21102,414,1,1,21102,277,1,0,1106,0,579,3,575,1008,575,89,570,1008,575,121,575,1,575,570,575,3,574,1008,574,10,570,1006,570,291,104,10,21101,0,1182,1,21101,0,313,0,1105,1,622,1005,575,327,1101,0,1,575,21102,327,1,0,1105,1,786,4,438,99,0,1,1,6,77,97,105,110,58,10,33,10,69,120,112,101,99,116,101,100,32,102,117,110,99,116,105,111,110,32,110,97,109,101,32,98,117,116,32,103,111,116,58,32,0,12,70,117,110,99,116,105,111,110,32,65,58,10,12,70,117,110,99,116,105,111,110,32,66,58,10,12,70,117,110,99,116,105,111,110,32,67,58,10,23,67,111,110,116,105,110,117,111,117,115,32,118,105,100,101,111,32,102,101,101,100,63,10,0,37,10,69,120,112,101,99,116,101,100,32,82,44,32,76,44,32,111,114,32,100,105,115,116,97,110,99,101,32,98,117,116,32,103,111,116,58,32,36,10,69,120,112,101,99,116,101,100,32,99,111,109,109,97,32,111,114,32,110,101,119,108,105,110,101,32,98,117,116,32,103,111,116,58,32,43,10,68,101,102,105,110,105,116,105,111,110,115,32,109,97,121,32,98,101,32,97,116,32,109,111,115,116,32,50,48,32,99,104,97,114,97,99,116,101,114,115,33,10,94,62,118,60,0,1,0,-1,-1,0,1,0,0,0,0,0,0,1,20,20,0,109,4,2101,0,-3,586,21001,0,0,-1,22101,1,-3,-3,21101,0,0,-2,2208,-2,-1,570,1005,570,617,2201,-3,-2,609,4,0,21201,-2,1,-2,1105,1,597,109,-4,2106,0,0,109,5,1202,-4,1,630,20102,1,0,-2,22101,1,-4,-4,21102,1,0,-3,2208,-3,-2,570,1005,570,781,2201,-4,-3,653,20102,1,0,-1,1208,-1,-4,570,1005,570,709,1208,-1,-5,570,1005,570,734,1207,-1,0,570,1005,570,759,1206,-1,774,1001,578,562,684,1,0,576,576,1001,578,566,692,1,0,577,577,21102,1,702,0,1106,0,786,21201,-1,-1,-1,1105,1,676,1001,578,1,578,1008,578,4,570,1006,570,724,1001,578,-4,578,21101,0,731,0,1105,1,786,1106,0,774,1001,578,-1,578,1008,578,-1,570,1006,570,749,1001,578,4,578,21101,0,756,0,1105,1,786,1105,1,774,21202,-1,-11,1,22101,1182,1,1,21102,1,774,0,1105,1,622,21201,-3,1,-3,1105,1,640,109,-5,2105,1,0,109,7,1005,575,802,21001,576,0,-6,21002,577,1,-5,1106,0,814,21102,0,1,-1,21101,0,0,-5,21102,0,1,-6,20208,-6,576,-2,208,-5,577,570,22002,570,-2,-2,21202,-5,53,-3,22201,-6,-3,-3,22101,1479,-3,-3,1201,-3,0,843,1005,0,863,21202,-2,42,-4,22101,46,-4,-4,1206,-2,924,21102,1,1,-1,1106,0,924,1205,-2,873,21101,35,0,-4,1105,1,924,1201,-3,0,878,1008,0,1,570,1006,570,916,1001,374,1,374,2101,0,-3,895,1102,1,2,0,1202,-3,1,902,1001,438,0,438,2202,-6,-5,570,1,570,374,570,1,570,438,438,1001,578,558,922,20102,1,0,-4,1006,575,959,204,-4,22101,1,-6,-6,1208,-6,53,570,1006,570,814,104,10,22101,1,-5,-5,1208,-5,53,570,1006,570,810,104,10,1206,-1,974,99,1206,-1,974,1101,0,1,575,21102,1,973,0,1106,0,786,99,109,-7,2105,1,0,109,6,21102,0,1,-4,21102,0,1,-3,203,-2,22101,1,-3,-3,21208,-2,82,-1,1205,-1,1030,21208,-2,76,-1,1205,-1,1037,21207,-2,48,-1,1205,-1,1124,22107,57,-2,-1,1205,-1,1124,21201,-2,-48,-2,1105,1,1041,21101,-4,0,-2,1105,1,1041,21102,-5,1,-2,21201,-4,1,-4,21207,-4,11,-1,1206,-1,1138,2201,-5,-4,1059,2101,0,-2,0,203,-2,22101,1,-3,-3,21207,-2,48,-1,1205,-1,1107,22107,57,-2,-1,1205,-1,1107,21201,-2,-48,-2,2201,-5,-4,1090,20102,10,0,-1,22201,-2,-1,-2,2201,-5,-4,1103,2101,0,-2,0,1106,0,1060,21208,-2,10,-1,1205,-1,1162,21208,-2,44,-1,1206,-1,1131,1105,1,989,21101,439,0,1,1105,1,1150,21102,477,1,1,1105,1,1150,21102,514,1,1,21102,1149,1,0,1105,1,579,99,21101,0,1157,0,1105,1,579,204,-2,104,10,99,21207,-3,22,-1,1206,-1,1138,1201,-5,0,1176,1201,-4,0,0,109,-6,2106,0,0,36,7,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,44,9,44,1,1,1,44,9,44,1,5,1,46,1,5,1,46,1,5,1,44,9,44,1,1,1,50,1,1,1,50,1,1,1,50,1,1,11,40,1,11,1,14,9,1,11,5,1,7,9,10,1,7,1,1,1,15,1,7,1,3,1,3,1,10,1,7,1,1,1,15,1,7,1,3,1,3,1,10,1,7,1,1,1,15,1,7,1,3,1,3,1,10,1,7,1,1,1,15,7,1,1,3,1,3,1,10,1,7,1,1,1,21,1,1,1,3,1,3,1,10,11,21,1,1,1,3,7,16,1,23,1,1,1,7,1,1,1,16,7,17,1,1,1,7,11,14,1,17,1,1,1,9,1,7,1,14,1,9,11,9,1,7,1,14,1,9,1,7,1,11,1,7,1,14,1,9,1,7,1,11,1,7,1,14,1,9,1,7,1,11,1,7,1,14,1,9,1,7,11,1,9,14,1,9,1,17,1,24,11,17,1,52,1,46,9,44,1,5,1,46,1,5,1,46,1,5,1,44,9,44,1,1,1,44,9,44,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,1,5,1,46,7,18
";
	}
}