﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2019
{
    public class SparseField<T>
    {
        public readonly Dictionary<(int x, int y), T> field = new Dictionary<(int x, int y), T>();

        public Func<T, char> SpriteMap = b => b switch {_ => '?'};

        public (int x, int y) CurPos { get; set; }

        public T CurValue
        {
            get => this[CurPos];
            set => this[CurPos] = value;
        }

        public T this[(int x, int y) index]
        {
            get
            {
                field.TryGetValue(index, out var result);
                return result;
            }
            set => field[index] = value;
        }

        public int MaterializedCount => field.Count;

        public override string ToString()
        {
            var (minX, maxX, minY, maxY) = GetDimensions();
            var result = new StringBuilder();
            for (var y = minY; y <= maxY; y++)
            {
                for (var x = minX; x <= maxX; x++)
                    result.Append(SpriteMap(this[(x, y)]));
                result.AppendLine();
            }
            return result.ToString();
        }

        public (int minX, int maxX, int minY, int maxY) GetDimensions()
        {
            int minX = int.MaxValue, maxX = int.MinValue, minY = int.MaxValue, maxY = int.MinValue;
            foreach (var (x, y) in field.Keys)
            {
                if (x < minX) minX = x;
                if (x > maxX) maxX = x;
                if (y < minY) minY = y;
                if (y > maxY) maxY = y;
            }
            return (minX, maxX, minY, maxY);
        }

        public Dir CurDir { get; set; }

        public (int x, int y) Move() => GetNextCoord(CurDir);

        public (int x, int y) GetNextCoord(Dir dir) => GetNextCoord(CurPos, dir);
        public (int x, int y) GetNextCoord((int x, int y) pos, Dir dir)
            => dir switch
            {
                Dir.U => (pos.x, pos.y - 1),
                Dir.D => (pos.x, pos.y + 1),
                Dir.L => (pos.x - 1, pos.y),
                Dir.R => (pos.x + 1, pos.y),
                _ => throw new InvalidOperationException("Unknown direction " + dir),
            };

        public static Dir TurnLeft(Dir dir) => (Dir)(((int)dir+ 3) % 4);
        public static Dir TurnRight(Dir dir) => (Dir)(((int)dir + 1) % 4);
        public Dir TurnLeft() => TurnLeft(CurDir);
        public Dir TurnRight() => TurnRight(CurDir);

        public enum Dir { U, R, D, L, }
    }
}