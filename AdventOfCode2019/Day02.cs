﻿using System;
using System.Buffers;
using System.Threading.Tasks;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 02: 1202 Program Alarm")]
	public class Day02
	{
		[TestCase("1,9,10,3,2,3,11,0,99,30,40,50", ExpectedResult = "3500,9,10,70,2,3,11,0,99,30,40,50")]
		[TestCase("1,0,0,0,99", ExpectedResult = "2,0,0,0,99")]
		[TestCase("2,3,0,3,99", ExpectedResult = "2,3,0,6,99")]
		[TestCase("2,4,4,5,99,0", ExpectedResult = "2,4,4,5,99,9801")]
		[TestCase("1,1,1,4,99,5,6,0,99", ExpectedResult = "30,1,1,4,2,5,6,0,99")]
		public string Intermediate1(string input)
		{
			var vm = new IntcodeVM(input);
			vm.Execute();
			return string.Join(',', vm.GetProgramState());
		}

		[TestCase(RealInput, ExpectedResult = 5305097)]
		public long Part1(string input)
		{
			var vm = new IntcodeVM(input)
			{
				[1] = 12,
				[2] = 02
			};
			vm.Execute();
			return vm[0];
		}

		[TestCase(RealInput, 19690720, ExpectedResult = 4925)]
		public int Part2(string input, int target)
		{
			var program = new IntcodeVM(input).GetProgramState();
			int result = -1;
			var pool = ArrayPool<long>.Create(program.Length, Environment.ProcessorCount * 2);
			Parallel.For(0, 100, (noun, state) =>
			{
				var tmp = pool.Rent(program.Length);
				for (var verb = 0; verb < 100; verb++)
				{
					Buffer.BlockCopy(program, 0, tmp, 0, program.Length * sizeof(long));
					var vm = new IntcodeVM(tmp)
					{
						[1] = noun,
						[2] = verb,
					};
					vm.Execute();
					if (vm[0] == target)
					{
						result = noun * 100 + verb;
						state.Break();
					}
				}
				pool.Return(tmp);
			});
			if (result < 0)
				throw new InvalidOperationException("Program couldn't satisfy the requirements for any input");
			return result;
		}

		private const string RealInput = "1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,13,19,1,9,19,23,1,6," +
										 "23,27,2,27,9,31,2,6,31,35,1,5,35,39,1,10,39,43,1,43,13,47," +
										 "1,47,9,51,1,51,9,55,1,55,9,59,2,9,59,63,2,9,63,67,1,5,67," +
										 "71,2,13,71,75,1,6,75,79,1,10,79,83,2,6,83,87,1,87,5,91,1," +
										 "91,9,95,1,95,10,99,2,9,99,103,1,5,103,107,1,5,107,111,2," +
										 "111,10,115,1,6,115,119,2,10,119,123,1,6,123,127,1,127,5," +
										 "131,2,9,131,135,1,5,135,139,1,139,10,143,1,143,2,147,1," +
										 "147,5,0,99,2,0,14,0";
	}
}