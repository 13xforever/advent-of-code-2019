﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 14: Space Stoichiometry")]
	public class Day14
	{

		[TestCase(TestInput1, ExpectedResult = 31)]
		[TestCase(TestInput2, ExpectedResult = 165)]
		[TestCase(TestInput3, ExpectedResult = 13312)]
		[TestCase(TestInput4, ExpectedResult = 180697)]
		[TestCase(TestInput5, ExpectedResult = 2210736)]
		[TestCase(RealInput, ExpectedResult = 362713)]
		public int Part1(string input)
		{
			var book = ParseReactionList(input);
			return (int)CalcChainCost("FUEL", 1, book);
		}

		[TestCase(TestInput3, ExpectedResult = 82892753)]
		[TestCase(TestInput4, ExpectedResult = 5586022)]
		[TestCase(TestInput5, ExpectedResult = 460664)]
		[TestCase(RealInput, ExpectedResult = 3281820)]
		public int Part2(string input)
		{
			const ulong targetOre = 1_000_000_000_000;
			var book = ParseReactionList(input);
			var below = (fuel: 0ul, ore: 0ul);
			var over = (fuel: 0ul, ore: 0ul);
			var cur = (fuel: 1ul, ore: 0ul);
			var @try = 0;
			try
			{
				do
				{
					@try++;
					cur.ore = CalcChainCost("FUEL", cur.fuel, book);
					if (cur.ore == targetOre)
						return (int)cur.fuel;

					if (cur.ore < targetOre)
					{
						if (below.fuel > 0 && cur.fuel + 1 == over.fuel)
							return (int)cur.fuel;

						var oreToFuel = cur.ore / cur.fuel;
						var c = (targetOre - cur.ore) / oreToFuel;
						below = cur;
						if (c > 1)
							cur = (cur.fuel + c, 0ul);
						else
							cur.fuel++;
					}
					else
					{
						if (over.fuel > 0 && cur.fuel - 1 == below.fuel)
							return (int)cur.fuel;

						var oreToFuel = cur.ore / cur.fuel;
						var c = (cur.ore - targetOre) / oreToFuel;
						over = cur;
						if (c > 1)
							cur = (cur.fuel - c, 0ul);
						else
							cur.fuel--;
					}
				} while (over.fuel == 0 || below.fuel == 0 || over.fuel - below.fuel > 1);
			}
			finally
			{
				Console.WriteLine($"Found solution in {@try} tries");
			}
			return (int)below.fuel;
		}

		private static Dictionary<string, Reaction> ParseReactionList(string input)
		{
			var result = input
				.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
				.Select(l => new Reaction(l))
				.ToDictionary(e => e.Result.Name, e => e);
			var ingredients = new HashSet<string>(
				from r in result.Values
				from c in r.Components
				select c.Name
			);
			var reactions = new HashSet<string>(result.Keys);
			reactions.ExceptWith(ingredients);
			Assert.That(reactions.Count, Is.EqualTo(1));
			var root = reactions.First();
			var lvl = 0;
			var list = new HashSet<string>(result[root].Components.Select(c => c.Name));
			do
			{
				var nxtList = new HashSet<string>();
				foreach (var cn in list)
				{
					if (result.TryGetValue(cn, out var c))
					{
						c.ChainLevel = Math.Max(c.ChainLevel, lvl + 1);
						result[cn] = c;
						foreach (var nlc in c.Components)
							nxtList.Add(nlc.Name);
					}
				}
				lvl++;
				list = nxtList;
			} while (list.Any());
			return result;
		}

		private ulong CalcChainCost(string ingredientName, ulong neededAmount, Dictionary<string, Reaction> reactions)
		{
			var result = new Dictionary<string, Ingredient>
			{
				[ingredientName] = new Ingredient(ingredientName, neededAmount)
			};
			bool updated;
			do
			{
				updated = false;
				//Console.WriteLine("Lowering...");
				result = Lower(result, reactions);
				//Console.WriteLine("Reducing...");
				var minLvl = int.MaxValue;
				foreach (var i in result.Values)
					if (reactions.TryGetValue(i.Name, out var r))
						minLvl = Math.Min(minLvl, r.ChainLevel);
				if (minLvl < int.MaxValue)
				{
					var newResult = new Dictionary<string, Ingredient>(result.Count);
					foreach (var i in result.Values)
					{
						if (reactions.TryGetValue(i.Name, out var r) && r.ChainLevel == minLvl)
						{
							updated = true;
							foreach (var c in r.Components)
							{
								newResult.TryGetValue(c.Name, out var ci);
								newResult[c.Name] = new Ingredient(c.Name, c.Amount + ci.Amount);
							}
						}
						else
						{
							newResult.TryGetValue(i.Name, out var ci);
							newResult[i.Name] = new Ingredient(i.Name, i.Amount + ci.Amount);
						}
					}
					result = newResult;
				}
				//Console.WriteLine(string.Join(", ", result.Values));
			} while (updated && result.Count > 1);
			//Assert.That(result.Count, Is.EqualTo(1));
			return result.Values.First().Amount;
		}

		private static Dictionary<string, Ingredient> Lower(Dictionary<string, Ingredient> list, Dictionary<string, Reaction> reactions)
		{
			bool updated;
			do
			{
				updated = false;
				var result = new Dictionary<string, Ingredient>();
				foreach (var i in list.Values.OrderBy(si => reactions.TryGetValue(si.Name, out var sr) ? sr.ChainLevel : int.MaxValue))
				{
					// not the raw material
					if (reactions.TryGetValue(i.Name, out var chain))
					{
						var mult = i.Amount / chain.Result.Amount;
						if (mult > 0)
						{
							updated = true;
							foreach (var c in chain.Components)
							{
								result.TryGetValue(c.Name, out var ci);
								result[c.Name] = new Ingredient(c.Name, c.Amount * mult + ci.Amount);
							}
						}
						var rem = i.Amount % chain.Result.Amount;
						if (rem > 0)
						{
							result.TryGetValue(i.Name, out var ci);
							result[i.Name] = new Ingredient(i.Name, rem + ci.Amount);
						}
					}
					else
					{
						result.TryGetValue(i.Name, out var ci);
						result[i.Name] = new Ingredient(i.Name, i.Amount + ci.Amount);
					}
				}
				list = result;
				//if (updated)
				//	Console.WriteLine(string.Join(", ", result.Values));
			} while (updated);
			return list;
		}

		private struct Reaction
		{
			public Reaction(string entry)
			{
				var parts = entry.Split(new[] {'=', '>'}, StringSplitOptions.RemoveEmptyEntries);
				Result = new Ingredient(parts[1]);
				Components = parts[0].Split(',', StringSplitOptions.RemoveEmptyEntries).Select(p => new Ingredient(p)).ToArray();
				ChainLevel = 0;
			}

			public readonly Ingredient Result;
			public readonly Ingredient[] Components;
			public int ChainLevel;

			public override string ToString() => $"#{ChainLevel}: {Result} = {string.Join(",", Components)}";
		}

		private struct Ingredient
		{
			public Ingredient(string entry)
			{
				var parts = entry.Split(' ', StringSplitOptions.RemoveEmptyEntries);
				Amount = ulong.Parse(parts[0]);
				Name = parts[1];
			}

			public Ingredient(string name, ulong amount)
			{
				Name = name;
				Amount = amount;
			}

			public readonly ulong Amount;
			public readonly string Name;

			public override string ToString() => $"{Amount} {Name}";
		}

		private const string TestInput1 = @"
10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL
";

		private const string TestInput2 = @"
9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL
";

		private const string TestInput3 = @"
157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT
";

		private const string TestInput4 = @"
2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF
";

		private const string TestInput5 = @"
171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX
";

		private const string RealInput = @"
5 LKQCJ, 1 GDSDP, 2 HPXCL => 9 LVRSZ
5 HPXCL, 5 PVJGF => 3 KZRTJ
7 LVRSZ, 2 GFSZ => 5 FRWGJ
9 ZPTXL, 5 HGXJH, 9 LQMT => 7 LVCXN
2 LQMT, 2 PVJGF, 10 CKRVN => 9 VWJS
2 VRMXL, 12 NBRCS, 2 WSXN => 7 GDSDP
1 CKRP => 8 TBHVH
1 SVMNB, 2 KZRTJ => 8 WKGQS
6 LKQCJ, 8 HPXCL, 7 MPZH => 1 BQPG
1 RCWL => 7 MPZH
4 FGCMS, 2 LQMT, 1 LKQCJ => 1 KTBRM
1 ZTCSK, 6 CXQB, 2 ZBZRT => 3 PVJGF
7 DBNLM => 9 ZBZRT
5 BGNQ, 2 WBPD, 5 KTBRM => 9 GFSZ
6 XQBHG, 1 GPWVC => 8 CKFTS
1 XWLQM, 29 XQBHG, 7 KPNWG => 5 BXVL
6 TBHVH, 1 KTBRM => 7 HJGR
1 LQMT, 14 KPNWG => 7 GPWVC
18 LVCXN, 8 XVLT, 4 KPNWG, 13 LKQCJ, 12 MFJFW, 5 GZNJZ, 1 FLFT, 7 WBPD => 8 KZGD
1 TBHVH => 1 VWKJ
118 ORE => 2 CKRP
2 LTCQX => 3 XQBHG
1 GPWVC => 4 SMFQ
6 CKRP => 4 RCWL
39 LHZMD, 15 CKFTS, 26 HVBW, 57 KTBRM, 13 DFCM, 30 KZGD, 35 FPNB, 1 LKQCJ, 45 HJGR, 22 RCZS, 34 VWKJ => 1 FUEL
1 BQPG, 2 BGNQ, 12 WBPD => 8 LTCQX
2 WSXN => 2 HPXCL
3 GRFPX => 5 XVLT
1 LVRSZ => 3 SVMNB
6 HLMT => 9 ZPTXL
20 GFSZ => 5 GZNJZ
1 RCWL => 9 KPNWG
24 BGNQ, 31 KTBRM => 8 FLFT
14 VSVG => 9 DBNLM
191 ORE => 8 CXQB
115 ORE => 2 SWVLZ
17 KZRTJ, 13 KPNWG => 7 CKRVN
9 BQPG => 4 XWLQM
4 SMFQ, 2 GRFPX => 1 MFJFW
6 CXQB, 4 CKRP, 2 BXVL, 5 GZNJZ, 3 VWJS, 1 FLFT, 4 KPNWG => 7 DFCM
1 TBHVH => 6 BGNQ
3 LQMT => 7 HLMT
11 GDSDP => 4 WBPD
2 KPNWG, 5 VWJS, 33 NBRCS => 7 NVDW
5 GDSDP => 6 FGCMS
1 GPWVC, 7 BGNQ, 1 FRWGJ => 8 GRFPX
23 KTBRM, 11 VRMXL, 6 GPWVC => 5 SRJHK
2 XQBHG, 1 GZNJZ => 3 HVBW
1 ZTCSK => 4 WSXN
1 XVLT, 5 HLMT, 1 ZPTXL, 2 HVBW, 7 NVDW, 1 WKGQS, 1 LTCQX, 5 MPZH => 3 FPNB
16 SRJHK => 6 DWBW
1 SVMNB, 1 VRMXL => 3 HGXJH
133 ORE => 6 VSVG
3 NBRCS, 1 FGCMS => 4 LQMT
1 CKRP => 4 ZTCSK
5 CKRVN, 1 FLFT => 1 RCZS
4 ZTCSK, 15 RCWL => 9 LKQCJ
1 SWVLZ => 8 NBRCS
5 CKRP, 14 CXQB => 5 VRMXL
1 SMFQ, 1 DWBW => 2 LHZMD
";
	}
}