﻿using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 07: Amplification Circuit")]
	public class Day07
	{
		[TestCase("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", ExpectedResult = "4,3,2,1,0")]
		[TestCase("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",ExpectedResult="0,1,2,3,4")]
		[TestCase("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0",ExpectedResult = "1,0,4,3,2")]
		[TestCase(RealInput, ExpectedResult = "3,2,4,1,0")]
		public string Intermediate1(string input)
			=> RunSim(input, 0, 4).coefficients;

		[TestCase("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", ExpectedResult = "9,8,7,6,5")]
		[TestCase("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", ExpectedResult = "9,7,8,5,6")]
		[TestCase(RealInput, ExpectedResult = "7,6,5,8,9")]
		public string Intermediate2(string input)
			=> RunSim(input, 5, 9).coefficients;

		[TestCase("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", ExpectedResult = 43210)]
		[TestCase("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",ExpectedResult=54321)]
		[TestCase("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0",ExpectedResult = 65210)]
		[TestCase(RealInput, ExpectedResult = 116680)]
		public long Part1(string input)
			=> RunSim(input, 0, 4).maxOutput;

		[TestCase("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", ExpectedResult = 139629729)]
		[TestCase("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10",ExpectedResult= 18216)]
		[TestCase(RealInput, ExpectedResult = 89603079)]
		public long Part2(string input)
			=> RunSim(input, 5, 9).maxOutput;

		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		private static (long maxOutput, string coefficients) RunSim(string input, int coefficientFrom, int coefficientTo)
		{
			var program = new IntcodeVM(input).GetProgramState();
			var programPool = ArrayPool<long>.Create();
			var coefPool = ArrayPool<byte>.Create();
			var maxOutput = -1L;
			var maxCoefficients = new byte[5];
			var lockObj = new object();
			var range = coefficientTo - coefficientFrom + 1;
			var searchSpace = range * range; // ^2
			searchSpace *= searchSpace; // ^4
			searchSpace *= range; // ^5
			// Parallel.For is ~11x slower for some reason
			var searchTasks = Enumerable.Range(0, searchSpace).Select(async idx => 
									{
										var coefficients = coefPool.Rent(5);
										var tmp = idx;
										for (var i = 0; i < 5; i++)
										{
											coefficients[i] = (byte)((tmp % range) + coefficientFrom);
											tmp /= range;
										}
										if (coefficients.Take(5).Distinct().Count() == 5)
										{
											var amplifiers = new Thread[5];
											var pipes = new BlockingCollection<long>[5];
											for (var i = 0; i < 5; i++)
											{
												var curAmpIdx = i;
												pipes[curAmpIdx] = new BlockingCollection<long>(2) { coefficients[curAmpIdx] };
											}
											pipes[0].Add(0);
											for (var amplifierIdx = 0; amplifierIdx < 5; amplifierIdx++)
											{
												var curAmpIdx = amplifierIdx;
												amplifiers[curAmpIdx] = new Thread(() =>
														{
															try
															{
																var programCopy = programPool.Rent(program.Length);
																Buffer.BlockCopy(program, 0, programCopy, 0, program.Length * sizeof(long));
																var vm = new IntcodeVM(programCopy)
																{
																	OnDataIn = _ => pipes[curAmpIdx].Take(),
																	OnDataOut = r => pipes[(curAmpIdx + 1) % 5].Add(r),
																};
																vm.Execute();
																programPool.Return(programCopy);
															}
															catch (Exception e)
															{
																Assert.Fail(e.ToString());
															}
														});
												amplifiers[curAmpIdx].Start();
											}
											for (var i = 0; i < 5; i++)
												amplifiers[i].Join();
											var signalLevel = pipes[0].Take();
											if (signalLevel > maxOutput)
											{
												lock (lockObj)
												{
													if (signalLevel > maxOutput)
													{
														maxOutput = signalLevel;
														Buffer.BlockCopy(coefficients, 0, maxCoefficients, 0, 5);
													}
												}
											}
											foreach (var p in pipes)
												p.Dispose();
										}
										coefPool.Return(coefficients);
									}).ToList();
			Task.WhenAll(searchTasks).GetAwaiter().GetResult();
			return (maxOutput, string.Join(",", maxCoefficients));
		}

		private const string RealInput = @"
3,8,1001,8,10,8,105,1,0,0,21,30,47,60,81,102,183,264,345,426,99999,3,9,1002,9,5,9,4,9,99,3,9,1002,9,5,9,1001,9,4,9,1002,9,4,9,4,9,99,3,9,101,2,9,9,1002,9,4,9,4,9,99,3,9,1001,9,3,9,1002,9,2,9,101,5,9,9,1002,9,2,9,4,9,99,3,9,102,4,9,9,101,4,9,9,1002,9,3,9,101,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,99
";
	}
}