﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace AdventOfCode2019
{
	[DebuggerDisplay("{OpCodeDisasm}")]
	public class IntcodeVM
	{
		private const int ScratchRamSize = 1*1024*1024;
		private readonly long[] program;
		private readonly Dictionary<int, long> ram = new Dictionary<int, long>();

		public IntcodeVM(long[] program)
		{
			this.program = new long[ScratchRamSize];
			Buffer.BlockCopy(program, 0, this.program, 0, program.Length*sizeof(long));
		}

		public IntcodeVM(string program) => this.program = program.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(long.Parse).ToArray();

		public delegate long DataInHandler(IntcodeVM vm);
		public delegate void DataOutHandler(long value);

		public DataInHandler OnDataIn;
		public DataOutHandler OnDataOut;

		private int ip;
		private int rb;
		private bool halt = false;

		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		public void Execute()
		{
			var modes = new OpMode[3];
			while (ip < program.Length && program[ip] != (int)OpCode.Halt && !halt)
			{
				var instruction = program[ip];
				var opCode = (OpCode)(instruction % 100);
				instruction /= 100;
				for (var i = 0; i < 3; i++)
				{
					modes[i] = (OpMode)(instruction % 10);
					instruction /= 10;
				}
				switch (opCode)
				{
					case OpCode.Add:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						var arg1 = GetValue(ip + 2, modes[1]);
						SetValue(ip + 3, modes[2], arg0 + arg1);
						ip += 4;
						break;
					}
					case OpCode.Mul:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						var arg1 = GetValue(ip + 2, modes[1]);
						SetValue(ip + 3, modes[2], arg0 * arg1);
						ip += 4;
						break;
					}
					case OpCode.In:
					{
						var arg0 = OnDataIn(this);
						SetValue(ip + 1, modes[0], arg0);
						ip += 2;
						break;
					}
					case OpCode.Out:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						OnDataOut(arg0);
						ip += 2;
						break;
					}
					case OpCode.Jnz:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						var arg1 = GetValue(ip + 2, modes[1]);
						ip = (int)(arg0 == 0 ? ip + 3 : arg1);
						break;
					}
					case OpCode.Jz:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						var arg1 = GetValue(ip + 2, modes[1]);
						ip = (int)(arg0 == 0 ? arg1 : ip + 3);
						break;
					}
					case OpCode.Lt:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						var arg1 = GetValue(ip + 2, modes[1]);
						SetValue(ip + 3, modes[2], arg0 < arg1 ? 1 : 0);
						ip += 4;
						break;
					}
					case OpCode.Equ:
					{
						var arg0 = GetValue(ip + 1, modes[0]);
						var arg1 = GetValue(ip + 2, modes[1]);
						SetValue(ip + 3, modes[2], arg0 == arg1 ? 1 : 0);
						ip += 4;
						break;
					}
					case OpCode.ModRB:
					{
						var arg0 = (int)GetValue(ip + 1, modes[0]);
						rb += arg0;
						ip += 2;
						break;
					}
					case OpCode.Halt:
						break;
					default:
						throw new InvalidOperationException("Unknown opcode " + program[ip]);
				}
			}
		}

		public void Halt() => halt = true;

		[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
		private long GetRamValue(int addr)
		{
			if (addr < 0)
				throw new InvalidOperationException("Can't get value from RAM address " + addr);

			if (addr < program.Length)
				return program[addr];

			ram.TryGetValue(addr, out var result);
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
		private void SetRamValue(int addr, long value)
		{
			if (addr < 0)
				throw new InvalidOperationException("Can't set value to RAM address " + addr);

			if (addr < program.Length)
				program[addr] = value;
			else
				ram[addr] = value;
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
		private long GetValue(int addr, OpMode mode)
		{
			var val = GetRamValue(addr);
			return mode switch
			{
				OpMode.Mem => GetRamValue((int)val),
				OpMode.Imm => val,
				OpMode.Rel => GetRamValue((int)(rb + val)),
				_ => throw new InvalidOperationException("Invalid opcode mode " + mode),
			};
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
		private long? TryGetValue(int addr, OpMode mode)
		{
			try
			{
				return GetValue(addr, mode);
			}
			catch
			{
				return null;
			}
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
		private void SetValue(int addr, OpMode mode, long value)
		{
			switch (mode)
			{
				case OpMode.Mem:
				case OpMode.Imm: // should be ignored
				{
					SetRamValue((int)GetRamValue(addr), value);
					break;
				}
				case OpMode.Rel:
				{
					SetRamValue((int)(rb + GetRamValue(addr)), value);
					break;
				}
				default:
					throw new InvalidOperationException("Invalid opcode mode " + mode);
			}
		}

		public long[] GetProgramState() => program.ToArray();

		public long this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
			get => program[index];
			[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
			set => program[index] = value;
		}

		public int ProgramLength => program.Length;


		private enum OpCode : byte
		{
			Add = 1,
			Mul = 2,
			In = 3,
			Out = 4,
			Jnz = 5,
			Jz = 6,
			Lt = 7,
			Equ = 8,
			ModRB = 9,
			Halt = 99,
		}

		private enum OpMode : byte
		{
			Mem = 0,
			Imm = 1,
			Rel = 2,
		}

		public string OpCodeDisasm => DisasmOpCode(ip).OpCodeDisasm;
		public string ProgramDisasm => DisasmProgram();

		private string DisasmProgram()
		{
			var vip = 0;
			var result = new StringBuilder();
			while (vip < program.Length)
			{
				var currentLine = vip == ip;
				var (len, line) = DisasmOpCode(vip, currentLine);
				result.Append(currentLine ? ">" : "　").AppendLine(line);
				vip += len;
			}
			return result.ToString();
		}

		private (byte len, string OpCodeDisasm) DisasmOpCode(int addr, bool comment = true)
		{
			static string FormatArg(long value, OpMode mod)
				=> mod switch
				{
					OpMode.Mem => $"[{value}]",
					OpMode.Imm => $"{value}",
					OpMode.Rel => $"RB{value:+0;0;0}",
					_ => $"{mod}:{value}",
				};

			var modes = ArrayPool<OpMode>.Shared.Rent(3);
			var instruction = program[addr];
			var opCode = (OpCode)(instruction % 100);
			instruction /= 100;
			for (var i = 0; i < 3; i++)
			{
				modes[i] = (OpMode)(instruction % 10);
				instruction /= 10;
			}
			var result = $"{addr:0000}: {opCode} ";

			var arg1 = FormatArg(addr + 1, modes[0]);
			var arg2 = FormatArg(addr + 2, modes[1]);
			var arg3 = FormatArg(addr + 3, modes[2]);
			var val1 = TryGetValue(addr + 1, modes[0]);
			var val2 = TryGetValue(addr + 2, modes[1]);
			var val3 = TryGetValue(addr + 3, modes[2]);
			var val1s = val1.HasValue ? val1.ToString() : "???";
			var val2s = val2.HasValue ? val2.ToString() : "???";
			var val3s = val3.HasValue ? val3.ToString() : "???";
			result += (opCode switch
			{
				OpCode.Add => $"{arg1}, {arg2} ⇒ {arg3}" + (comment ? $" ; {val1s} + {val2s} ({val3s} ⇒ {(val1 + val2)?.ToString() ?? "???"})" : ""),
				OpCode.Mul => $"{arg1}, {arg2} ⇒ {arg3}" + (comment ? $" ; {val1s} * {val2s} ({val3s} ⇒ {(val1 * val2)?.ToString() ?? "???"})" : ""),
				OpCode.In => $"{arg1} ⇐" + (comment ? $" ; {val1s} ⇐" : ""),
				OpCode.Out => $"{arg1} ⇒" + (comment ? $" ; {val1s} ⇒" : ""),
				OpCode.Jnz => $"{arg1}, {arg2}" + (comment ? $" ; {(val1.HasValue ? (val1 != 0 ? "Jump to " + val2s : "nop") : val1s)}" : ""),
				OpCode.Jz => $"{arg1}, {arg2}" + (comment ? $" ; {(val1.HasValue ? (val1 == 0 ? "Jump to " + val2s : "nop") : val1s)}" : ""),
				OpCode.Lt => $"{arg1}, {arg2} ⇒ {arg3}" + (comment ? $" ; {val1s} < {val2s} ({val3s} ⇒ {(val1.HasValue && val2.HasValue ? (val1 < val2 ? "1" : "0") : "???")})" : ""),
				OpCode.Equ => $"{arg1}, {arg2} ⇒ {arg3}" + (comment ? $" ; {val1s} = {val2s} ({val3s} ⇒ {(val1.HasValue && val2.HasValue ? (val1 == val2 ? "1" : "0") : "???")})" : ""),
				OpCode.ModRB => $"{arg1}" + (comment ? $" ; RB: {rb} ⇒ {(val1.HasValue ? (rb + val1).ToString() : val1s)}" : ""),
				_ => "",
			});

			var len = opCode switch
			{
				OpCode.Add => 4,
				OpCode.Mul => 4,
				OpCode.In => 2,
				OpCode.Out => 2,
				OpCode.Jnz => 3,
				OpCode.Jz => 3,
				OpCode.Lt => 4,
				OpCode.Equ => 4,
				OpCode.ModRB => 2,
				OpCode.Halt => 1,
				_ => 1,
			};
			return ((byte)len, result);
		}
	}
}