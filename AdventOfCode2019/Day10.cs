﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 10: Monitoring Station")]
	public class Day10
	{
		[TestCase(TestInput1, ExpectedResult = "3,4,8")]
		[TestCase(TestInput2, ExpectedResult = "5,8,33")]
		[TestCase(TestInput3, ExpectedResult = "1,2,35")]
		[TestCase(TestInput4, ExpectedResult = "6,3,41")]
		[TestCase(TestInput5, ExpectedResult = "11,13,210")]
		public string Intermediate1(string input)
		{
			var result = AnalyzeAsteroids(input);
			return $"{result.x},{result.y},{result.visible}";
		}

		[TestCase(RealInput, ExpectedResult = 280)]
		public int Part1(string input)
		{
			var result = AnalyzeAsteroids(input);
			return result.visible;
		}


		[TestCase(TestInput5, ExpectedResult = 802)]
		[TestCase(RealInput, ExpectedResult = 706)]
		public int Part2(string input)
		{
			var info = AnalyzeAsteroids(input);
			var vaporList = Vaporize(info.list, (info.x, info.y));
			Assert.That(vaporList.Count, Is.GreaterThan(200));
			var (x, y) = vaporList[199];
			return x * 100 + y;
		}


		private static (int x, int y, int visible, (int x, int y)[] list) AnalyzeAsteroids(string input)
		{
			var field = input.Split(Day01.SeparatorCharacters, StringSplitOptions.RemoveEmptyEntries).ToArray();
			var maxX = field[0].Length;
			var maxY = field.Length;
			var tmpList = new List<(int x, int y)>();
			for (var y = 0; y < maxY; y++)
			for (var x = 0; x < maxX; x++)
			{
				if (field[y][x] == '#')
					tmpList.Add((x, y));
			}
			var asteroidList = tmpList.ToArray();
			var total = asteroidList.Length;
			var bestGuess = (x: -1, y: -1, visible: -1);
			var uniqueAngles = new HashSet<double>();
			foreach (var a in asteroidList)
			{
				uniqueAngles.Clear();
				var chkd = 0;
				foreach (var b in asteroidList)
				{
					if (a == b)
						continue;

					uniqueAngles.Add(Math.Atan2(b.y - a.y, b.x - a.x));
					chkd++;
					if (total - chkd + uniqueAngles.Count <= bestGuess.visible)
						break;
				}
				if (uniqueAngles.Count > bestGuess.visible)
					bestGuess = (a.x, a.y, uniqueAngles.Count);

				if (bestGuess.visible == total)
					break;
			}
			return (bestGuess.x, bestGuess.y, bestGuess.visible, asteroidList);
		}

		private static List<(int x, int y)> Vaporize((int x, int y)[] asteroidList, (int x, int y) baseCoord)
		{
			var map = asteroidList
				.Where(a => a != baseCoord)
				.Select(a => (a.x, a.y, angle: Normalize(Math.Atan2(a.y - baseCoord.y, a.x - baseCoord.x)) , dist: Math.Abs(a.y - baseCoord.y) + (a.x - baseCoord.x)))
				.GroupBy(v => v.angle)
				.ToDictionary(g => g.Key, g => g.OrderBy(v => v.dist).ToList());
			var result = new List<(int x, int y)>(asteroidList.Length);
			do
			{
				var angles = map.Keys.OrderBy(a => a).ToList();
				foreach (var a in angles)
				{
					result.Add((map[a][0].x, map[a][0].y));
					if (map[a].Count == 1)
						map.Remove(a);
					else
						map[a].RemoveAt(0);
				}
			} while (map.Count > 0);
			return result;
		}

		private static double Normalize(double atan2)
		{
			if (atan2 < -Math.PI/2)
				return Math.PI * 2.5 + atan2;
			return atan2 + Math.PI / 2;
		}

		private const string TestInput1 = @"
.#..#
.....
#####
....#
...##
";

		private const string TestInput2 = @"
......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####
";

		private const string TestInput3 = @"
#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.
";

		private const string TestInput4 = @"
.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..
";

		private const string TestInput5 = @"
.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
";

		private const string RealInput = @"
.###.#...#.#.##.#.####..
.#....#####...#.######..
#.#.###.###.#.....#.####
##.###..##..####.#.####.
###########.#######.##.#
##########.#########.##.
.#.##.########.##...###.
###.#.##.#####.#.###.###
##.#####.##..###.#.##.#.
.#.#.#####.####.#..#####
.###.#####.#..#..##.#.##
########.##.#...########
.####..##..#.###.###.#.#
....######.##.#.######.#
###.####.######.#....###
############.#.#.##.####
##...##..####.####.#..##
.###.#########.###..#.##
#.##.#.#...##...#####..#
##.#..###############.##
##.###.#####.##.######..
##.#####.#.#.##..#######
...#######.######...####
#....#.#.#.####.#.#.#.##
";
	}
}