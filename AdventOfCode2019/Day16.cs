﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 16: Flawed Frequency Transmission")]
	public class Day16
	{
		[TestCase("98765", "123", ExpectedResult = "96160")]
		public string Intermediate1(string sequence, string pattern)
		{
			var result = Mul(ParseNum(sequence), ParseNum(pattern));
			return new string(result.Select(b => (char)(b + (byte)'0')).ToArray());
		}

		[TestCase("12345678", 1, ExpectedResult = "48226158")]
		[TestCase("12345678", 2, ExpectedResult = "34040438")]
		[TestCase("12345678", 3, ExpectedResult = "03415518")]
		[TestCase("12345678", 4, ExpectedResult = "01029498")]
		[TestCase("80871224585914546619083218645595", 100, ExpectedResult = "24176176")]
		[TestCase("19617804207202209144916044189917", 100, ExpectedResult = "73745418")]
		[TestCase("69317163492948606335995924319873", 100, ExpectedResult = "52432133")]
		[TestCase(RealInput, 100, ExpectedResult = "59281788")]
		public string Part1(string input, int phases)
		{
			var seq = ParseNum(input);
			var result = ApplyFft(seq, phases);
			return new string(result.Take(8).Select(b => (char)(b + (byte)'0')).ToArray());
		}

		[TestCase("03036732577212944063491565474664", ExpectedResult = "84462026")]
		[TestCase("02935109699940807407585447034323", ExpectedResult = "78725270")]
		[TestCase("03081770884921959731165446850517", ExpectedResult = "53553731")]
		[TestCase(RealInput, ExpectedResult = "96062868")]
		public string Part2(string input)
		{
			var seq = ParseNum(input);
			var offset = 0;
			foreach (var b in seq.Take(7))
				offset = offset * 10 + b;
			const int coef = 10_000;
			Assert.That(seq.Length * coef/2, Is.LessThanOrEqualTo(offset));

			var realInput = new byte[seq.Length * coef];
			for (var i=0; i < coef; i++)
				Buffer.BlockCopy(seq, 0, realInput, i* seq.Length, seq.Length);
			realInput = realInput.Skip(offset).ToArray();
			var result = ApplyFftReverse(realInput, 100);
			return new string(result.Take(8).Select(b => (char)(b + (byte)'0')).ToArray());
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		private static byte[] ApplyFft(byte[] sequence, int phases)
		{
			var result = new byte[sequence.Length];
			for (var p = 0; p < phases; p++)
			{
				for (var i=0; i < sequence.Length; i++)
				{
					var j = i;
					var offset = i + 1;
					long acc = 0;
					while (j < sequence.Length)
					{
						for (var k = j; k < j + offset && k < sequence.Length; k++)
							acc += sequence[k];
						j += offset * 2;
						for (var k = j; k < j + offset && k < sequence.Length; k++)
							acc -= sequence[k];
						j += offset * 2;
					}
					result[i] = (byte)(Math.Abs(acc) % 10);
				}
				var tmp = result;
				result = sequence;
				sequence = tmp;
			}
			return sequence;
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization)]
		private static byte[] ApplyFftReverse(byte[] sequence, int phases)
		{
			var result = new byte[sequence.Length];
			Buffer.BlockCopy(sequence, 0, result, 0, sequence.Length);
			for (var _ = 0; _ < phases; _++)
			for (var i = sequence.Length - 2; i >= 0; i--)
				result[i] = (byte)((result[i] + result[i + 1]) % 10);
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveOptimization | MethodImplOptions.AggressiveInlining)]
		private static sbyte GetPattern(int phase, int pos) => basePattern[((pos + 1) / phase) % 4];

		private static readonly sbyte[] basePattern = {0, 1, 0, -1};

		private byte[] ParseNum(string input) => input.Trim().ToCharArray().Select(c => (byte)(c - '0')).ToArray();

		private static byte[] Mul(byte[] sequence, byte[] pattern)
		{
			var pl = pattern.Length;
			for (var i = 0; i < sequence.Length; i++)
				sequence[i] = (byte)(((int)sequence[i] * pattern[i % pl]) % 10);
			return sequence;
		}

		private const string RealInput = @"
59750939545604170490448806904053996019334767199634549908834775721405739596861952646254979483184471162036292390420794027064363954885147560867913605882489622487048479055396272724159301464058399346811328233322326527416513041769256881220146486963575598109803656565965629866620042497176335792972212552985666620566167342140228123108131419565738662203188342087202064894410035696740418174710212851654722274533332525489527010152875822730659946962403568074408253218880547715921491803133272403027533886903982268040703808320401476923037465500423410637688454817997420944672193747192363987753459196311580461975618629750912028908140713295213305315022251918307904937
";
	}
}