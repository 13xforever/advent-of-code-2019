﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 24: Planet of Discord")]
	public class Day24
	{
		[TestCase(TestInput, ExpectedResult = 2129920)]
		[TestCase(RealInput, ExpectedResult = 32506764)]
		public long Part1(string input)
		{
			var states = new Dictionary<Ecosystem, int>(16384);
			var step = 0;
			var s = new Ecosystem(input);
			int cycleStart;
			while (!states.TryGetValue(s, out cycleStart))
			{
				Console.WriteLine("Step " + step);
				Console.WriteLine(s.ToString());
				states[s] = step++;
				s = s.Cycle();
			}
			Console.WriteLine($"Cycle start: {cycleStart}, current step: {step}, length: {step-cycleStart}");
			Console.WriteLine(s.ToString());
			return (int)s;
		}

		[TestCase(TestInput, 1, ExpectedResult = 27)]
		[TestCase(TestInput, 10, ExpectedResult = 99)]
		[TestCase(RealInput, 200, ExpectedResult = 1963)]
		public long Part2(string input, int steps)
		{
			var s = new FoldedEcosystem(input);
			Console.WriteLine($"Step 0, layer 0:");
			Console.WriteLine(s.ToString());

			for (var i = 0; i < steps; i++)
			{
				s = s.Cycle();
				/*
				for (var l = -i - 1; l < i + 2; l++)
				{
					Console.WriteLine($"Step {i + 1}, layer {l}:");
					Console.WriteLine(s.ToString(l));
				}
				*/
			}
			return s.BugCount;
		}

		[Flags]
		private enum BugColony : int
		{
			B0 = 0b_00000_00000_00000_00000_00000,
			B1 = 0b_00000_00000_00000_00000_00000,
			B2 = 0b_00000_00000_00000_00000_00000,
			B3 = 0b_00000_00000_00000_00000_00000,
			B4 = 0b_00000_00000_00000_00000_00000,
			
			B5 = 0b_00000_00000_00000_00000_00000,
			B6 = 0b_00000_00000_00000_00000_00000,
			B7 = 0b_00000_00000_00000_00000_00000,
			B8 = 0b_00000_00000_00000_00000_00000,
			B9 = 0b_00000_00000_00000_00000_00000,
			
			B10 = 0b_00000_00000_00000_00000_00000,
			B11 = 0b_00000_00000_00000_00000_00000,
			B12 = 0b_00000_00000_00000_00000_00000,
			B13 = 0b_00000_00000_00000_00000_00000,
			B14 = 0b_00000_00000_00000_00000_00000,
			
			B15 = 0b_00000_00000_00000_00000_00000,
			B16 = 0b_00000_00000_00000_00000_00000,
			B17 = 0b_00000_00000_00000_00000_00000,
			B18 = 0b_00000_00000_00000_00000_00000,
			B19 = 0b_00000_00000_00000_00000_00000,
			
			B20 = 0b_00000_00000_00000_00000_00000,
			B21 = 0b_00000_00000_00000_00000_00000,
			B22 = 0b_00000_00000_00000_00000_00000,
			B23 = 0b_00000_00000_00000_00000_00000,
			B24 = 0b_00000_00000_00000_00000_00000,
		}

		private struct Ecosystem
		{
			public Ecosystem(string input)
			{
				var field = 0;
				var b = 1;
				var cells = input.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).SelectMany(l => l.ToCharArray());
				foreach (var c in cells)
				{
					if (c == '#')
						field |= b;
					b <<= 1;
				}
				state = (BugColony)field;
			}

			private Ecosystem(BugColony state) { this.state = state; }

			private readonly BugColony state;

			private static void Inflate(int state, bool[] flags)
			{
				for (byte i = 0; i < 25; i++, state >>= 1)
					flags[i] = (state & 1) == 1;
			}

			private static int Deflate(bool[] flags)
			{
				var result = 0;
				for (byte i = 0; i < 25; i++)
					result = (result >> 1) | (flags[i] ? 0b_10000_00000_00000_00000_00000 : 0);
				return result;
			}

			private static byte GetNeighbourCount(byte idx, bool[] flags)
			{
				byte result = (byte)(idx % 5 == 0 ? 0 : flags[idx - 1] ? 1 : 0);
				result += (byte)(idx < 5 ? 0 : flags[idx - 5] ? 1 : 0);
				result += (byte)(idx % 5 == 4 ? 0 : flags[idx + 1] ? 1 : 0);
				result += (byte)(idx > 19 ? 0 : flags[idx + 5] ? 1 : 0);
				return result;
			}

			public Ecosystem Cycle()
			{
				var flags = ArrayPool<bool>.Shared.Rent(25);
				var result = ArrayPool<bool>.Shared.Rent(25);
				Inflate((int)state, flags);
				for (byte i = 0; i < 25; i++)
				{
					var nc = GetNeighbourCount(i, flags);
					var b = flags[i];
					var rb = (b && nc == 1) || !b && (nc == 1 || nc == 2);
					result[i] = rb;
				}
				ArrayPool<bool>.Shared.Return(flags);
				var rf =  new Ecosystem((BugColony)Deflate(result));
				ArrayPool<bool>.Shared.Return(result);
				return rf;
			}

			public static explicit operator int(Ecosystem s) => (int)s.state;
			public static explicit operator Ecosystem(int s) => new Ecosystem((BugColony)s);

			public override int GetHashCode() => state.GetHashCode();
			public override bool Equals(object? obj) => obj is Ecosystem e && Equals(e);
			public bool Equals(Ecosystem other) => other.state == state;

			public override string ToString()
			{
				var s = ArrayPool<bool>.Shared.Rent(25);
				Inflate((int)state, s);
				var result = new StringBuilder(25+Environment.NewLine.Length*5);
				for (var l = 0; l < 5; l++)
				{
					for (var i = 0; i < 5; i++)
						result.Append(s[l * 5 + i] ? '#' : '.');
					result.AppendLine();
				}
				ArrayPool<bool>.Shared.Return(s);
				return result.ToString();
			}
		}

		private struct FoldedEcosystem
		{
			public FoldedEcosystem(string input)
			{
				var field = 0;
				var b = 1;
				var cells = input.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).SelectMany(l => l.ToCharArray());
				foreach (var c in cells)
				{
					if (c == '#')
						field |= b;
					b <<= 1;
				}
				state = new [] { (BugColony)field};
			}

			private FoldedEcosystem(BugColony[] state) { this.state = state; }

			private readonly BugColony[] state;

			private static void Inflate(int state, byte[] flags)
			{
				for (byte i = 0; i < 25; i++, state >>= 1)
					flags[i] = (byte)(state & 1);
			}

			private static int Deflate(byte[] flags)
			{
				var result = 0;
				for (byte i = 25; i > 0; i--)
					result = (result << 1) | flags[i-1];
				return result;
			}

			private byte GetNeighbourCount(int idx, byte[] layerUp, byte[] layerCur, byte[] layerDown)
			{
				if (idx == 12)
					return 0;

				//left
				var result = idx % 5 == 0 ? layerUp[11] : idx == 13 ? layerDown[4] + layerDown[9] + layerDown[14] + layerDown[19] + layerDown[24] : layerCur[idx - 1];
				//up
				result += idx < 5 ? layerUp[7] : idx == 17 ? layerDown[20] + layerDown[21] + layerDown[22] + layerDown[23] + layerDown[24] : layerCur[idx - 5];
				//right
				result += idx % 5 == 4 ? layerUp[13] : idx==11 ? layerDown[0] + layerDown[5] + layerDown[10] + layerDown[15] + layerDown[20] : layerCur[idx + 1];
				//down
				result += idx > 19 ? layerUp[17] : idx==7 ? layerDown[0] + layerDown[1] + layerDown[2] + layerDown[3] + layerDown[4] : layerCur[idx + 5];
				return (byte)result;
			}

			public FoldedEcosystem Cycle()
			{
				var oldLayerUp = ArrayPool<byte>.Shared.Rent(25);
				var oldLayer = ArrayPool<byte>.Shared.Rent(25);
				var oldLayerDown = ArrayPool<byte>.Shared.Rent(25);
				var newLayer = ArrayPool<byte>.Shared.Rent(25);
				Inflate(0, oldLayerDown);
				Inflate(0, oldLayer);
				var result = new BugColony[state.Length + 2];
				for (var layer = 0; layer < result.Length; layer++)
				{
					Inflate((int)this[layer], oldLayerUp);
					for (byte i = 0; i < 25; i++)
					{
						var nc = GetNeighbourCount(i, oldLayerUp, oldLayer, oldLayerDown);
						var b = oldLayer[i];
						var rb = (b == 1 && nc == 1) || (b == 0 && (nc == 1 || nc == 2));
						newLayer[i] = (byte)(rb ? 1 : 0);
					}
					result[layer] = (BugColony)Deflate(newLayer);
					var tmp = oldLayerDown;
					oldLayerDown = oldLayer;
					oldLayer = oldLayerUp;
					oldLayerUp = tmp;
				}
				ArrayPool<byte>.Shared.Return(oldLayerUp);
				ArrayPool<byte>.Shared.Return(oldLayer);
				ArrayPool<byte>.Shared.Return(oldLayerDown);
				ArrayPool<byte>.Shared.Return(newLayer);
				return new FoldedEcosystem(result);
			}

			public BugColony this[int idx] => idx < 0 || idx >= state.Length ? 0 : state[idx];

			public int BugCount => (int)state.Sum(b => Popcnt.PopCount((uint)b));

			public override string ToString() => ToString(0);
			public string ToString(int layer)
			{
				var s = ArrayPool<byte>.Shared.Rent(25);
				Inflate((int)this[layer + state.Length/2], s);
				var result = new StringBuilder(25+Environment.NewLine.Length*5);
				for (var l = 0; l < 5; l++)
				{
					for (var i = 0; i < 5; i++)
					{
						var idx = l * 5 + i;
						result.Append(idx == 12 ? '?' : s[idx] == 1 ? '#' : '.');
					}
					result.AppendLine();
				}
				ArrayPool<byte>.Shared.Return(s);
				return result.ToString();
			}
		}

		private const string TestInput = @"
....#
#..#.
#..##
..#..
#....
";

		private const string RealInput = @"
.#.##
..##.
##...
#...#
..###
";
	}
}