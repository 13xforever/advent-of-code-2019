﻿using System;
using System.Numerics;
using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 22: Slam Shuffle")]
	public class Day22
	{
		[Test]
		public void TechTests1()
		{
			var d = new Deck(10).DealInto();
			Assert.That(d.ToString(), Is.EqualTo("9 8 7 6 5 4 3 2 1 0"));

			d = new Deck(10).Cut(3);
			Assert.That(d.ToString(), Is.EqualTo("3 4 5 6 7 8 9 0 1 2"));

			d = new Deck(10).Cut(-4);
			Assert.That(d.ToString(), Is.EqualTo("6 7 8 9 0 1 2 3 4 5"));

			d = new Deck(10).DealWithIncrement(3);
			Assert.That(d.ToString(), Is.EqualTo("0 7 4 1 8 5 2 9 6 3"));
		}

		[Test]
		public void TechTests2()
		{
			var d = new TrackingDeck(10, 0).DealInto();
			Assert.That(d.CardPos, Is.EqualTo(9));
			d = new TrackingDeck(10, 9).DealInto();
			Assert.That(d.CardPos, Is.EqualTo(0));

			d = new TrackingDeck(10, 0).Cut(3);
			Assert.That(d.CardPos, Is.EqualTo(7));
			d = new TrackingDeck(10, 9).Cut(3);
			Assert.That(d.CardPos, Is.EqualTo(6));

			d = new TrackingDeck(10, 0).Cut(-4);
			Assert.That(d.CardPos, Is.EqualTo(4));
			d = new TrackingDeck(10, 9).Cut(-4);
			Assert.That(d.CardPos, Is.EqualTo(3));

			d = new TrackingDeck(10, 0).DealWithIncrement(3);
			Assert.That(d.CardPos, Is.EqualTo(0));
			d = new TrackingDeck(10, 9).DealWithIncrement(3);
			Assert.That(d.CardPos, Is.EqualTo(7));
		}

		[TestCase(TestInput1, ExpectedResult = "0 8 5 2 10 7 4 1 9 6 3")]
		[TestCase(TestInput2, ExpectedResult = "9 1 4 7 10 2 5 8 0 3 6")]
		[TestCase(TestInput3, ExpectedResult = "8 4 0 7 3 10 6 2 9 5 1")]
		[TestCase(TestInput4, ExpectedResult = "1 8 4 0 7 3 10 6 2 9 5")]
		public string Intermediate1(string input) => new Deck(11).Shuffle(input).ToString();

		[TestCase(TestInput1, ExpectedResult = 0)]
		[TestCase(TestInput2, ExpectedResult = 8)]
		[TestCase(TestInput3, ExpectedResult = 2)]
		[TestCase(TestInput4, ExpectedResult = 3)]
		public long Intermediate2(string input) => new TrackingDeck(11, 0).Shuffle(input, 1).CardPos;

		[TestCase(RealInput, 10007, 2019, ExpectedResult = 3749)]
		[TestCase(RealInput, 119315717514047, 2020, ExpectedResult = 90301448897578)]
		public long Intermediate3(string input, long length, long cardNumber) => new TrackingDeck(length, cardNumber).Shuffle(input, 1).CardPos;

		[TestCase(TestInput1, ExpectedResult = 0)]
		[TestCase(TestInput2, ExpectedResult = 9)]
		[TestCase(TestInput3, ExpectedResult = 8)]
		[TestCase(TestInput4, ExpectedResult = 1)]
		public long Intermediate4(string input) => new TrackingDeck(11, 0).Shuffle(input, 1).InvCardPos;

		[TestCase(RealInput, ExpectedResult = 3749)]
		public int Part1(string input)
		{
			var result = new Deck(10007).Shuffle(input);
			for(var i=0; i<result.Length;i++)
				if (result[i] == 2019)
					return i;
			throw new InvalidOperationException("You failed");
		}

		[TestCase(RealInput, 10007, 1, 3749, ExpectedResult = 2019)]
		[TestCase(RealInput, 10007, 2, 7955, ExpectedResult = 2019)]
		[TestCase(RealInput, 10007, 3, 6797, ExpectedResult = 2019)]
		[TestCase(RealInput, 119315717514047, 101741582076661, 2020, ExpectedResult = 77225522112241)]
		public long Part2(string input, long length,  long times, long cardPos) => new TrackingDeck(length, cardPos).Shuffle(input, times).InvCardPos;

		private class TrackingDeck
		{
			public readonly long Length;
			public long CardPos;
			public long InvCardPos;

			public TrackingDeck(long length, long trackCard)
			{
				Length = length;
				CardPos = trackCard;
			}

			public TrackingDeck DealInto()
			{
				CardPos = Length - CardPos - 1;
				return this;
			}

			public TrackingDeck Cut(long n)
			{
				if (n == 0 || n >= Length)
					return this;

				if (n < 0)
					n = Length + n;
				CardPos -= n;
				if (CardPos < 0)
					CardPos += Length;
				else if (CardPos >= Length)
					CardPos %= Length;
				return this;
			}

			public TrackingDeck DealWithIncrement(long n)
			{
				
				CardPos = (CardPos * n) % Length;
				return this;
			}

			public TrackingDeck Shuffle(string instructions, BigInteger times)
			{
				var lines = instructions
					.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
				BigInteger a = 1;
				BigInteger b = 0;
				foreach (var l in lines)
				{
					switch (l)
					{
						// length - (ax+b) - 1
						// length - ax - b - 1
						case "deal into new stack":
						{
							a = Length - a;
							b = Length - b - 1;
							break;
						}
						// (ax+b) - n
						// ax + b - n
						case string ci when ci.StartsWith("cut"):
						{
							var n = long.Parse(ci[4..]);
							if (n == 0 || n >= Length)
								break;

							if (n < 0)
								n = Length + n;
							b -= n;
							if (b < 0)
								b += Length;
							else if (b >= Length)
								b %= Length;
							break;
						}
						// (ax+b) * n
						// anx + bn
						case string dwi when dwi.StartsWith("deal with increment"):
						{
							var n = int.Parse(dwi[20..]);
							a = (a * n) % Length;
							b = (b * n) % Length;
							break;
						}
						default:
							throw new InvalidOperationException("Unknown shuffling instruction: " + l);
					}
				}
				Console.WriteLine($"Shuffle equation: {a} * x + {b}");

				// y = ax + b
				// ax = y - b
				// x = (1/a)*y - (1/a)*b
				var invA = InvMod(a, Length);
				var invB = ((Length - b) * invA)%Length;
				Console.WriteLine($"Reverse equation: {invA} * y + {invB}");

				// A = 1/a, B = -b/a
				// y[i-1] = (A * y[i] + B) // t times => geometric progression
				// A^t * y + (1 - A^t) / (1 - A) * B 
				var invAtoT = BigInteger.ModPow(invA, times, Length);
				var invResult = (invAtoT * CardPos + (1 - invAtoT) * InvMod(1 - invA + Length, Length) * invB) % Length;
				if (invResult < 0)
					invResult += Length;
				Console.WriteLine($"Inverse lookup result: {invResult}");
				InvCardPos = (long)invResult;
				CardPos = (long)((a * CardPos + b) % Length);
				return this;
			}

			private static BigInteger InvMod(BigInteger value, BigInteger modulus)
			{
				if (modulus == 1)
					return 0;

				var modDiv = modulus;
				BigInteger y = 0;
				BigInteger x = 1;
				while (value > 1)
				{
					var q = value / modDiv;
					var t = modDiv;
					modDiv = value % modDiv;
					value = t;
					t = y;
					y = x - q * y;
					x = t;
				}
				if (x < 0)
					x += modulus;
				return x;
			}
		}

		private class Deck
		{
			public readonly int Length;
			private int[] deck;
			private int[] backDeck;

			public Deck(int length)
			{
				this.Length = length;
				deck = new int[length];
				backDeck = new int[length];
				for (var i = 0; i < length; i++)
					deck[i] = i;
			}

			public Deck DealInto()
			{
				for (int i = 0, j = Length - 1; i < j; i++, j--)
				{
					var tmp = deck[i];
					deck[i] = deck[j];
					deck[j] = tmp;
				}
				return this;
			}

			public Deck Cut(int n)
			{
				if (n >= Length || n == 0)
					return this;

				if (n < 0)
					n = Length + n;

				Buffer.BlockCopy(deck, 0, backDeck, (Length - n)*sizeof(int), n * sizeof(int));
				Buffer.BlockCopy(deck, n*sizeof(int), backDeck, 0, (Length - n) * sizeof(int));
				var tmp = deck;
				deck = backDeck;
				backDeck = tmp;
				return this;
			}

			public Deck DealWithIncrement(int n)
			{
				for (int i = 0, j = 0; i < Length; i++, j = (j + n) % Length)
					backDeck[j] = deck[i];
				var tmp = deck;
				deck = backDeck;
				backDeck = tmp;
				return this;
			}

			public Deck Shuffle(string instructions)
			{
				var lines = instructions.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
				foreach (var l in lines)
				{
					switch (l)
					{
						case "deal into new stack":
						{
							DealInto();
							break;
						}
						case string ci when ci.StartsWith("cut"):
						{
							Cut(int.Parse(ci[4..]));
							break;
						}
						case string dwi when dwi.StartsWith("deal with increment"):
						{
							DealWithIncrement(int.Parse(dwi[20..]));
							break;
						}
						default:
							throw new InvalidOperationException("Unknown shuffle instructions: " + l);
					}
				}
				return this;
			}

			public int this[int index] => deck[index];

			public override string ToString() => string.Join(" ", deck);
		}

		private const string TestInput1 = @"
deal with increment 7
deal into new stack
deal into new stack
";

		private const string TestInput2 = @"
cut 6
deal with increment 7
deal into new stack
";

		private const string TestInput3 = @"
deal with increment 7
deal with increment 9
cut -2
";

		private const string TestInput4 = @"
deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1
";

		private const string RealInput = @"
cut 578
deal with increment 25
cut -3085
deal with increment 16
cut -6620
deal with increment 17
cut -1305
deal with increment 71
cut -4578
deal with increment 44
cut 5639
deal with increment 74
deal into new stack
deal with increment 39
cut 7888
deal with increment 17
deal into new stack
cut 6512
deal with increment 46
cut -8989
deal with increment 46
cut -8518
deal with increment 75
cut -870
deal into new stack
deal with increment 53
cut 7377
deal with increment 60
cut -4733
deal with increment 25
cut -6914
deal with increment 23
cut -4379
deal into new stack
cut 582
deal with increment 35
cut 9853
deal with increment 2
cut -142
deal with increment 74
cut 328
deal into new stack
deal with increment 75
deal into new stack
cut -8439
deal into new stack
deal with increment 34
cut 2121
deal with increment 2
cut 8335
deal with increment 65
cut -1254
deal into new stack
cut -122
deal with increment 75
cut -9227
deal into new stack
deal with increment 24
cut 3976
deal into new stack
deal with increment 8
cut -3292
deal with increment 4
deal into new stack
cut -8851
deal with increment 2
deal into new stack
cut 4333
deal with increment 73
deal into new stack
deal with increment 9
cut -7880
deal with increment 49
cut 9770
deal with increment 30
cut 2701
deal with increment 59
cut 4292
deal with increment 37
deal into new stack
cut -184
deal with increment 25
cut 9907
deal with increment 46
deal into new stack
cut 902
deal with increment 46
cut 2622
deal into new stack
cut 637
deal with increment 58
cut 7354
deal with increment 69
deal into new stack
deal with increment 49
deal into new stack
deal with increment 19
cut -8342
deal with increment 68
deal into new stack
";
	}
}