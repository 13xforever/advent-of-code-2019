﻿using NUnit.Framework;

namespace AdventOfCode2019
{
	[TestFixture(TestName = "Day 04: Secure Container")]
	public class Day04
	{
		[TestCase("122345-122345", ExpectedResult = 1)]
		[TestCase("122345-122346", ExpectedResult = 2)]
		[TestCase("206938-679128", ExpectedResult = 1653)]
		public int Part1(string input)
		{
			var nums = input.Split('-');
			var low = new Number(nums[0]);
			var high = new Number(nums[1]);
			var result = 0;
			for (var i = low; i <= high; i++)
			{
				if (i.IsValid)
					result++;
			}
			return result;
		}

		[TestCase("122345-122345", ExpectedResult = 1)]
		[TestCase("122345-122346", ExpectedResult = 2)]
		[TestCase("206938-679128", ExpectedResult = 1133)]
		public int Part2(string input)
		{
			var nums = input.Split('-');
			var low = new Number(nums[0]);
			var high = new Number(nums[1]);
			var result = 0;
			for (var i = low; i <= high; i++)
			{
				if (i.IsValid2)
					result++;
			}
			return result;
		}

		[TestCase("122345", ExpectedResult = true)]
		[TestCase("111111", ExpectedResult = true)]
		[TestCase("223450", ExpectedResult = false)]
		[TestCase("123789", ExpectedResult = false)]
		public bool IsValid(string num) => new Number(num).IsValid;

		[TestCase("112233", ExpectedResult = true)]
		[TestCase("123444", ExpectedResult = false)]
		[TestCase("111122", ExpectedResult = true)]
		public bool IsValid2(string num) => new Number(num).IsValid2;



		public struct Number
		{
			public Number(string num)
			{
				Digits = new byte[6];
				var n = int.Parse(num);
				for (byte j = 6; j > 0; j--)
				{
					Digits[j - 1] = (byte)(n % 10);
					n /= 10;
				}
			}

			public readonly byte[] Digits;

			public bool IsValid
			{
				get
				{
					var result = false;
					for (var i = 0; i < 5; i++)
						if (Digits[i] > Digits[i + 1])
							return false;
						else if (Digits[i] == Digits[i + 1])
							result = true;
					return result;
				}
			}

			public bool IsValid2
			{
				get
				{
					var result = false;
					var d = 255;
					var c = 0;
					for (var i = 0; i < 6; i++)
					{
						if (i < 5 && Digits[i] > Digits[i + 1])
							return false;

						var digit = Digits[i];
						if (digit == d)
							c++;
						else
						{
							if (c == 2)
								result = true;
							c = 1;
							d = digit;
						}
					}
					return result || c == 2;
				}
			}

			public override bool Equals(object obj) { return obj is Number other && Equals(other); }

			public bool Equals(Number other)
			{
				for (var i = 0; i < 6; i++)
					if (Digits[i] != other.Digits[i])
						return false;
				return true;
			}

			public override int GetHashCode() => ((int)this).GetHashCode();

			public static bool operator ==(Number left, Number right) { return left.Equals(right); }
			public static bool operator !=(Number left, Number right) { return !left.Equals(right); }

			public static Number operator ++(Number n)
			{
				for (var i = 5; i >= 0; i--)
				{
					if (n.Digits[i] < 9)
					{
						n.Digits[i]++;
						return n;
					}
					n.Digits[i] = 0;
				}
				return n;
			}

			public static bool operator <=(Number n1, Number n2)
			{
				for (var i = 0; i < 6; i++)
					if (n1.Digits[i] != n2.Digits[i])
						return n1.Digits[i] < n2.Digits[i];
				return true;
			}

			public static bool operator >=(Number n1, Number n2)
			{
				for (var i = 0; i < 6; i++)
					if (n1.Digits[i] != n2.Digits[i])
						return n1.Digits[i] >= n2.Digits[i];
				return true;
			}

			public static implicit operator int(Number n)
			{
				var result = (int)n.Digits[0];
				for (var i = 1; i < 6; i++)
					result = result * 10 + n.Digits[i];
				return result;
			}
		}
	}
}