Advent of Code 2019 solutions
=============================

Solutions are implemented as unit tests using NUnit 3, to run the code you will need dotnet core sdk 3.1.

```
$ dotnet test
```

Day 23 Part 2 is known to be flaky ¯\\\_(ツ)\_/¯.

Day 25 fully automated generic solution is still pending. There's an interactive CLI in a separate branch.

Visualization branches require the new Windows Terminal to render properly on Windows.